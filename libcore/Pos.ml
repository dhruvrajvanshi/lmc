open Prelude
module Self = struct
    type t = { pos_line: int; pos_column: int }

    let line (p: t) = p.pos_line
    let column (p: t) = p.pos_column

    let make pos_line pos_column = { pos_line; pos_column }

    let show p = sprintf "%i:%i" (line p) (column p)

end
include Self
