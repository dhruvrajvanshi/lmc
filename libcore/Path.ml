open Prelude
type t = String.t

let is_directory = Caml.Sys.is_directory

let exists = Caml.Sys.file_exists

let extension path =
    match Base.String.rsplit2 ~on:'.' path with
    | Some((_, extn)) ->
        extn
    | None -> ""

let has_extension ~extn path =
    extension path = extn

let path_seperator =
    if String.equal Sys.os_type "Win32"
    then '\\'
    else '/'

let file_name path =
    let res = match String.rsplit2 ~on:path_seperator path with
    | Some((_, name)) -> name
    | None -> path
    in
    res

let chop_extension path =
    let extension = extension path in
    if String.length extension = 0
    then
        path
    else
        let suffix = "." ^ extension in
        String.chop_suffix_exn ~suffix path
    

let join components =
    let sep = path_seperator |> Char.to_string in
    String.concat ~sep components

let join_2 prefix suffix =
    let sep = path_seperator |> Char.to_string in
    String.concat ~sep [prefix; suffix]

