open Prelude

type t = { loc_span: Span.t; loc_path: Path.t }
type loc = t

let make ~path ~start ~stop = {
    loc_path = path;
    loc_span = Span.make start stop
}

let path l = l.loc_path

let span l = l.loc_span

let start = span >>> Span.start_pos

let stop = span >>> Span.stop_pos

module type HasLocImpl = sig
    type t
    val loc: t -> loc
end

module HasLoc(M: HasLocImpl) = struct
    let path = M.loc >>> path
    include Span.HasSpan(struct
        type t = M.t
        let span = M.loc >>> span
    end)
end

let show self = sprintf "%s (%s)" (path self) (start self |> Pos.show)


let between l1 l2 =
    let path = path l1 in
    let start = start l1 in
    let stop = stop l2 in
    make ~path ~start ~stop

