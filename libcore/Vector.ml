open Prelude


include Array

let empty = [||]

module Builder = struct
    type 'a t = {
        mutable array: 'a array;
        mutable length: int;
    }

    let empty () = { array = [||]; length = 0; }


    let length vec = vec.length

    let capacity vec = vec.array |> Array.length

    let push t vec = begin
        if length vec = capacity vec
        then begin
            if length vec = 0 then
                vec.array <- [|Obj.magic 0|]
            else
                let old = vec.array in
                vec.array <- Array.init (2 *(capacity vec))
                    ~f:(fun i -> if i < Array.length old
                    then old.(i)
                    else Obj.magic(0)
                );
        end;
        let old_len = vec.length in
        vec.length <- old_len + 1;
        Array.set vec.array old_len t
    end
    let singleton item =
        let b = empty () in
        push item b;

        b

    let to_array vec =
        Array.init (vec.length) ~f:(Array.get vec.array)
end

let of_builder = Builder.to_array

let singleton item = Builder.singleton item |> of_builder

let pp ?(sep=", ") show_item v =
    let s = fold v ~init:"" ~f:(fun prev c ->
        sprintf "%s%s%s" prev (show_item c) sep
    ) in
    let l = String.length s in
    if length v = 0
    then ""
    else
        String.slice s 0 (l - String.length sep)

