open Prelude

module Self = struct
    type t = { name: String.t; id: Int.t }
        [@@deriving sexp]

    let make id name = { id; name }

    let name x = x.name
    let id x = x.id

    let compare s1 s2 = (id s1) - (id s2)

    let hash = id >>> Int.hash

    let pp self =
        sprintf "Symbol(%s, %i)" (name self) (id self)

    let show = pp

    let equal s1 s2 = id s1 = id s2

    let eq = equal
    let ne s1 s2 = eq s1 s2 |> not

end
include Self
module Hashtbl = Hashtbl.Make(Self)
module Hash_set = Hash_set.Make(Self)


module Gen = struct
    type t = { mutable next_id: int }

    let make () = { next_id = 0 }

    let make_symbol self name =
        let id = self.next_id in
        self.next_id <- id + 1;
        Self.make id name
end
