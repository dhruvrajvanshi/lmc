
module type T = sig
    val type_ty_symbol: Symbol.t
    val int_ty_symbol: Symbol.t
    val bool_ty_symbol: Symbol.t
    val unit_ty_symbol: Symbol.t
    val unit_val_symbol: Symbol.t
    val true_symbol: Symbol.t
    val false_symbol: Symbol.t
    val lazy_ty_symbol: Symbol.t
end

module Make(M: sig
    val next_symbol: String.t -> Symbol.t
end): T = struct
    let type_ty_symbol = M.next_symbol "Type_t"
    let int_ty_symbol = M.next_symbol "Int_t"

    let bool_ty_symbol = M.next_symbol "Bool_t"

    let unit_ty_symbol = M.next_symbol "Unit_t"

    let true_symbol = M.next_symbol "true"

    let false_symbol = M.next_symbol "false"

    let unit_val_symbol = M.next_symbol "unit"
    let lazy_ty_symbol = M.next_symbol "Lazy_t"
end