open Prelude
module type Vertex = sig
    type t
    val compare: t -> t -> int
    val hash: t -> int
    val sexp_of_t: t -> Sexp.t
    val t_of_sexp: Sexp.t -> t
    val pp: t -> string
    val eq: t -> t -> bool
    val ne: t -> t -> bool
end

module type T = sig
    type vertex
    val vertices: vertex Vector.t
    val edges: (vertex * vertex) Vector.t
    module Vertex: Vertex with type t = vertex
end

module type Builder = sig
    type vertex
    val add_edge: parent:vertex -> child:vertex -> unit
    val add_vertex: vertex -> unit
    module Build(): T with type vertex = vertex
end


module Builder(V: Vertex): Builder with type vertex = V.t = struct
    type vertex = V.t

    module VHash_set = Hash_set.Make(V)

    let vertices = VHash_set.create ()
    let edges = Vector.Builder.empty ()

    let add_vertex v = Hash_set.add vertices v


    let add_edge ~parent ~child =
        add_vertex parent;
        add_vertex child;
        Vector.Builder.push (parent, child) edges

    module Build(): T with type vertex = vertex = struct
        type nonrec vertex = vertex
        let vertices = Hash_set.to_array vertices

        let edges = Vector.of_builder edges
        module Vertex = V
        module VertexHashtbl = Hashtbl.Make(V)
    end
end

let scc
    (type v)
    (module G: T with type vertex = v)
    : v Vector.t Vector.t =
    let vertices = G.vertices in
    let edges = G.edges in
    let _index = ref 0 in
    let _indices = Hashtbl.of_alist_exn (module G.Vertex) [] in
    let _lowlinks = Hashtbl.of_alist_exn (module G.Vertex) [] in
    let _on_stack = Hashtbl.of_alist_exn (module G.Vertex) [] in

    let index() = !_index in

    let sccs = Vector.Builder.empty () in

    let index_of v = Hashtbl.find _indices v in
    let lowlink_of v = Hashtbl.find _lowlinks v
        |> Option.value ~default:0 in
    let set_lowlink_of v data =
        if Hashtbl.mem _lowlinks v
        then
            Hashtbl.update _lowlinks v ~f:(fun _ -> data)
        else
            Hashtbl.add_exn _lowlinks
                ~key:v ~data
    in
    let set_index_of v data = Hashtbl.add_exn _indices
        ~key:v ~data
    in

    let is_on_stack v = Hashtbl.find _on_stack v
        |> Option.value ~default:false
    in

    let s = Stack.create () in
    let module S = struct
        let push v =
            Hashtbl.add_exn _on_stack ~key:v ~data:true;

            Stack.push v s

        let pop () =
            let v = Stack.pop s in
            if is_on_stack v then
                Hashtbl.remove _on_stack v;
            v
    end in
    let rec go () =
        Vector.iter vertices ~f:(fun v ->
            match index_of v with
            | Some(_) -> ()
            | None -> strongconnect v
        )
    
    and strongconnect v =
        let v_index = index () in
        set_index_of v v_index;
        set_lowlink_of v (index ());
        _index := index() + 1;
        S.push v;
        Vector.iter edges ~f:(fun (_v, w) ->
            if G.Vertex.eq _v v then
                match index_of w with
                | None ->
                    strongconnect w;
                    set_lowlink_of v (min (lowlink_of v) (lowlink_of w))
                | Some(w_index) ->
                    if is_on_stack w
                    then
                        set_lowlink_of v (min (lowlink_of v) w_index)
        );
        (* If v is a root node, pop the stack and generate an SCC *)
        if lowlink_of v = v_index
        then (
            (* start a new scc *)
            let scc = Vector.Builder.empty () in
            let _w = ref (S.pop()) in
            let w() = !_w in
            let pop = S.pop in
            let push w = scc |> Vector.Builder.push w in
            let rec loop (): unit =
                if G.Vertex.ne (w()) v
                then (
                    _w := pop ();
                    push (w());
                    loop ()
                )
            in
            push (w());
            loop ();
            let scc = Vector.of_builder scc in
            Vector.Builder.push scc sccs
        )
    in

    go ();

    let result = Vector.of_builder sccs in
    result
