open Prelude

type t = { span_start: Pos.t; span_stop: Pos.t; }
type span = t

let start_pos s = s.span_start
let stop_pos s = s.span_stop

let make span_start span_stop = { span_start; span_stop }

module type HasSpanImpl = sig
    type t
    val span: t -> span
end


module HasSpan(M: HasSpanImpl) = struct
    let start_pos = M.span >>> start_pos
    let stop_pos = M.span >>> stop_pos
end


