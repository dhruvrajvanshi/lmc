open Base

let (>>>) f1 f2 = fun x -> f1 x |> f2
(** Reverse function composition operator *)

let (||>) = (>>>)
(** Reverse function composition operator *)

let id = Fn.id

let sprintf = Caml.Format.sprintf

let printf = Caml.Format.printf


let panic s =
    printf "%s\n" s;
    failwith s

exception Panic of string

let panicf fmt = Caml.Format.ksprintf panic fmt

let ksprintf = Caml.Format.ksprintf

exception Unimplemented of string

let _todo str = raise (Unimplemented(str))
let todo fmt = ksprintf _todo fmt

let ensure cond msg =
    if cond
    then ()
    else
        raise (Panic(msg))

let ensuref cond fmt = Caml.Format.ksprintf (ensure cond) fmt




module Char = Base.Char


module Array = Base.Array

module Hashtbl = struct
    include Core_kernel.Hashtbl

    let add_exn ?(msg="Duplicate key") ~key ~data t =
        match add t ~key ~data with
        | `Ok -> ()
        | `Duplicate ->
            let sexp_of_key = sexp_of_key t key in
            panicf "Tried to add duplicate key %s: %s\n" (Sexp.to_string sexp_of_key) msg
end

module String = struct
    include Base.String

    module Hashtbl = Hashtbl.Make(String)

    module Hash_set = Core_kernel.Hash_set.Make(String)


    let slice =
        Core_kernel.String.slice
end


module StrHashtbl = String.Hashtbl
module Map = Core_kernel.Map
module Hash_set = Core_kernel.Hash_set
module List = Base.List
module Option = struct
    include Base.Option
    let or_else self ~f =
        match self with
        | Some(x) -> x
        | None -> f()
    
    let or_else_bind self ~f =
        match self with
        | Some(_) -> self
        | None -> f()
    let unwrap self =
        match self with
        | Some(value) -> value
        | None -> panic "Option.unwrap on None"

    let concat_map ~f self =
        self >>= f
end
module Int = struct
    include Core_kernel.Int
    module Hashtbl = Core_kernel.Hashtbl.Make(Core_kernel.Int)
end
module Set = Base.Set
module Sexp = Base.Sexp
