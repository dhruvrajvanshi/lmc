open Prelude

let read_dir path = path
    |> Caml.Sys.readdir
    |> Array.map ~f:(Path.join_2 path)

let read_to_string path =
    let ch = Caml.open_in path in
    let rec go acc =
        try
            go (acc ^ "\n" ^ Caml.input_line ch)
        with End_of_file ->
            acc
    in
    let inp = go "" in
    Caml.close_in ch;
    String.slice inp 1 (String.length inp)

let write_string path str =
    let ch = open_out path in
    Caml.output_string ch str;
    Caml.flush ch;
    Caml.close_out ch
