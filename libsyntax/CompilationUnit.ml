open LibCore.Prelude
open Syn

type t = compilation_unit

module V = Syn

let source_file meta sf = CUSourceFile(meta, sf)
let directory meta ~name ~units ~scope = CUDirectory(meta, Syn.{
    cu_dir_name = name;
    cu_dir_self_scope = scope;
    cu_dir_units = units;
})

let name = function
| CUSourceFile(_, sf) -> SourceFile.name sf
| CUDirectory(_, dir) -> CUDir.name dir



let self_scope = function
| CUSourceFile(_, sf) -> SourceFile.self_scope sf
| CUDirectory(_, dir) -> CUDir.self_scope dir

let meta = function
| CUSourceFile(m,  _) -> m
| CUDirectory(m, _) -> m

include Meta.HasMeta(struct
    type nonrec t = t
    let meta = meta
end)

let rec pp = function
| CUSourceFile(meta, sf) ->
    sprintf "// %s\n%s"
        (Meta.loc meta |> Loc.show)
        (SourceFile.pp sf)

| CUDirectory(meta, dir) ->
    List.(Hashtbl.to_alist (CUDir.units dir)
    |> map ~f:(snd >>> pp)
    |> fold ~init:"" ~f:(
        sprintf "// Directory <id: %i>\n%s\n%s" (Meta.id meta))
    )

let name = function
| CUSourceFile(_, sf) -> SourceFile.name sf
| CUDirectory(_, dir) -> dir.cu_dir_name


