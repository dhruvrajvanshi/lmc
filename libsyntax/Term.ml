open LibCore
open Prelude
open Syn

type t = term

module V = Syn

let meta = V.(function
| TVar(m, _) -> m
| TLInt(m, _) -> m
| TArrow(m, _) -> m
| TModule(m, _) -> m
| TExtern(m, _) -> m
| TProp(m, _) -> m
| TCall(m, _) -> m
| TIf(m, _) -> m
| TPanic(m, _) -> m
| TError(m) -> m
)

include Meta.HasMeta(struct
    type nonrec t = t
    let meta = meta
end)

module Prop = struct
    type t = tm_prop
    let lhs p = p.tm_prop_lhs

    let rhs p = p.tm_prop_rhs

    let make lhs rhs = {
        tm_prop_lhs = lhs;
        tm_prop_rhs = rhs;
    }

    let pp = tm_prop_pp
end

module Var = struct
    type t = tm_var

    let ident (TmVar(i)): Ident.t = i

    let make meta name = TVar(meta, TmVar(name))

    let pp (TmVar(i)) = Ident.pp i

end

module LInt = struct
    type t = tm_l_int
    let value v = v
    let make meta value = TLInt(meta, value)

    let pp = string_of_int
end


module Arrow = struct
    type t = tm_arrow

    module Param = struct
        type t = tm_arrow_param

        let label p = p.tm_arrow_param_label

        let ty_annotation p = p.tm_arrow_param_ty_annotation

        let pp = tm_arrow_param_pp

        let make label annotation: t = 
            {
                tm_arrow_param_label = label;
                tm_arrow_param_ty_annotation = annotation;
            }

        let labeled label annotation =
            make (Some label) annotation

        let unlabeled annotation =
            make None annotation

        let loc p =
            let stop = p |> ty_annotation
                |> TyAnnotation.loc in

            let start = p |> label
                |> Option.map ~f:BindingIdent.loc
                |> Option.value ~default:stop  in
            Loc.between start stop


    end

    let params a = a.tm_arrow_params
    let rhs a = a.tm_arrow_rhs

    let make scope params rhs = {
        tm_arrow_params = params;
        tm_arrow_rhs = rhs;
        tm_arrow_scope = scope;
    }

    let pp = tm_arrow_pp

    let self_scope t = t.tm_arrow_scope

end

module Extern = struct
    type t = tm_extern

    let ident (t: t): Ident.t = t
end

module Module = struct
    type t = tm_module

    let make scope decls = {
        tm_module_scope = scope;
        tm_module_decls = decls;
    }

    let declarations (m: t): decl Vector.t = m.tm_module_decls

    let self_scope m = m.tm_module_scope

    let pp = tm_l_module_pp
end

module Call = struct
    type t = tm_call
    let make func args =
        { tm_call_func = func; tm_call_args = args }
    let args c = c.tm_call_args
    let func c = c.tm_call_func

    let pp = tm_call_pp

    module Arg = struct
        type t = tm_call_arg
        let make label value =
            { tm_call_arg_label = label; tm_call_arg_value = value }

        let label arg = arg.tm_call_arg_label

        let value arg = arg.tm_call_arg_value

        let pp = tm_call_arg_pp
    end
end

module If = struct
    type t = tm_if
    let cond i = i.tm_if_cond

    let true_branch i = i.tm_if_t
    let false_branch i = i.tm_if_f

    let pp = tm_if_pp

    let make c t f =
        {
            tm_if_cond = c;
            tm_if_t = t;
            tm_if_f = f;
        }
end

module Panic = struct
    type t = tm_panic
    let make (tm: term option): t = tm

    let term (t: t): term option = t

end


module Error = struct
    let make meta = TError(meta)

    let pp _ = "<SyntaxError>"
end


let var = Var.make

let l_int = LInt.make

let arrow meta scope params rhs = TArrow(meta, Arrow.make scope params rhs)

let l_module meta scope decls = TModule(meta, Module.make scope decls)

let extern meta ident = TExtern(meta, ident)

let error = Error.make

let prop meta lhs rhs = TProp(meta, Prop.make lhs rhs)

let call meta func args = TCall(meta, Call.make func args)


let panic meta = TPanic(meta, (Panic.make None))

let panic_with_arg meta arg = TPanic(meta, (Panic.make (Some arg)))

let t_if meta pred t f =
    TIf(meta, If.make pred t f)

let pp = term_pp

let pp_annotation = function
| Some(t) -> sprintf ": %s" (pp t)
| None -> ""


