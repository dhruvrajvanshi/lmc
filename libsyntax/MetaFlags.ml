include Syn.Flags

let empty: t = of_int 0

let has_param_list = of_int 1

let contains (t: t) (flag: t) = ((to_int t) land (to_int flag)) > 0
