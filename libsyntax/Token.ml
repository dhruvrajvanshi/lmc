open LibCore.Prelude
type variant =
    | EOF
    | ID

    | DEF
    | MODULE
    | EXTERN
    | IF | ELSE
    | PANIC


    | UNDERSCORE

    | INT

    | SEMICOLON
    | LPAREN
    | RPAREN
    | COLON
    | DOT
    | COMMA
    | LBRACE
    | RBRACE

    | EQ
    | EQEQ
    | ARROW

    | ERROR
    [@@deriving show, eq, sexp, hash]

module Variant = struct
    module Self = struct
        type t = variant [@@deriving show, eq, sexp, hash]
        let compare = compare
        let sexp_of_t = sexp_of_t
        let t_of_sexp = t_of_sexp

    end
    include Self

    module Hash_set = Hash_set.Make(Self)
end
    

type t = { meta: Meta.t; lexeme: String.t; variant: variant }

let lexeme t = t.lexeme

let variant t = t.variant

let meta t = t.meta

let make ~meta ~lexeme ~variant = { meta; lexeme; variant }

let is self variant =
    self.variant = variant

include Meta.HasMeta(struct
    type nonrec t = t
    let meta = meta
end)
