open LibCore
open Prelude
open Syn

type t = ty_annotation

let term (TyAnnotation(_, t)): term = t
let meta (TyAnnotation(m, _)): meta = m

let pp = term >>> term_pp

let make meta tm = TyAnnotation(meta, tm)

include Meta.HasMeta(struct
    type t = ty_annotation
    let meta = meta
end)
