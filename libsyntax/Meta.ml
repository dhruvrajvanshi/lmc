open LibCore.Prelude
open Syn

type t = meta

let make ?(flags=MetaFlags.empty) ~id ~loc ~scope = {
    meta_loc = loc;
    meta_id = id;
    meta_scope = scope;
    meta_flags = flags;
}

let id m = m.meta_id
let loc m = m.meta_loc

let scope m = m.meta_scope

let meta (m: t): t = m

let flags (m: t) = m.meta_flags

let has_flag (m: t) (flag: MetaFlags.t) = 
    let flags = flags m in
    MetaFlags.contains flags flag

let show meta =
    sprintf "meta { id = %i }" (id meta)

module type HasMeta = sig
    type t
    val meta: t -> meta
end

module HasMeta(M: HasMeta) = struct
    let id = M.meta ||> id
    let loc = M.meta ||> loc

    let scope = M.meta ||> scope

    let flags = M.meta ||> flags

    let has_flag = M.meta ||> has_flag

    include Loc.HasLoc(struct
        type t = M.t
        let loc = loc
    end)
end
