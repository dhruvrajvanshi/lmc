open LibCore
open Prelude

module StrHashtbl = String.Hashtbl
type name = String.t [@@deriving eq, sexp, hash]

type loc = Loc.t

let name_pp x = x

module Flags: sig
    type t
    val of_int: int -> t
    val to_int: t -> int
end = struct
    type t = int
    let of_int i = i
    let to_int i = i
end

type meta = {
    meta_loc: loc;
    meta_id: int;
    meta_scope: Scope.t;
    meta_flags: Flags.t;
}
and node =
    | NTerm of term
    | NDecl of decl
    | NCompilationUnit of compilation_unit
    | NProgram of program

and ident = { ident_name: name; ident_meta: meta }
and binding_ident =
    | BIWildcard of meta * Scope.t
    | BIName of meta * Scope.t * ident

and tm_l_int = int

and tm_var = TmVar of ident

and ty_annotation  = TyAnnotation of meta * term

and tm_panic = term option

and term =
    | TLInt of meta * tm_l_int
    | TVar of meta * tm_var
    | TArrow of meta * tm_arrow
    | TModule of meta * tm_module
    | TExtern of meta * tm_extern
    | TProp of meta * tm_prop
    | TCall of meta * tm_call
    | TIf of meta * tm_if
    | TPanic of meta * tm_panic
    | TError of meta

and tm_if = {
    tm_if_cond: term;
    tm_if_t: term;
    tm_if_f: term;
}

and tm_call = {
    tm_call_func: term;
    tm_call_args: tm_call_args
}

and tm_call_args = tm_call_arg Vector.t
and tm_call_arg = {
    tm_call_arg_label: ident option;
    tm_call_arg_value: term;

}
and tm_prop = {
    tm_prop_lhs: term;
    tm_prop_rhs: ident;
}

and tm_extern = ident
and tm_arrow = {
    tm_arrow_params: tm_arrow_param Vector.t;
    tm_arrow_rhs: ty_annotation;
    tm_arrow_scope: Scope.t;
}
and tm_arrow_param = {
    tm_arrow_param_label: binding_ident option;
    tm_arrow_param_ty_annotation: ty_annotation;
}
and tm_module = {
    tm_module_decls: decl Vector.t;
    tm_module_scope: Scope.t;
}
and decl_def = {
    decl_def_name: binding_ident;
    decl_def_params: params;
    decl_def_annotation: ty_annotation option;
    decl_def_rhs: term;
    decl_def_scope: Scope.t;
}

and decl =
    | DDef of meta * decl_def
    | DError of meta

and params = param Vector.t
and param = {
    param_binding_ident: binding_ident;
    param_ty_annotation: ty_annotation Option.t;
}

and source_file = {
    sf_declarations: decl Vector.t;
    sf_name: string;
    sf_self_scope: Scope.t;
}

and compilation_unit
    = CUSourceFile of meta * source_file
    | CUDirectory of meta * cu_directory

and cu_directory = {
    cu_dir_self_scope: Scope.t;
    cu_dir_name: string;
    cu_dir_units: compilation_unit StrHashtbl.t;
}

and program = {
    prog_units: compilation_unit StrHashtbl.t;
    prog_scope: Scope.t;
}

let ident_pp (i: ident): String.t =
    i.ident_name

let tm_var_pp (TmVar i) = ident_pp i

let tm_l_int_pp (i: tm_l_int) = string_of_int i

let rec term_pp = function
| TVar(_, t) -> tm_var_pp t
| TLInt(_, i) -> tm_l_int_pp i
| TArrow(_, a) -> tm_arrow_pp a
| TModule(_, m) -> tm_l_module_pp m
| TExtern(_, e) -> tm_extern_pp e
| TProp(_, p) -> tm_prop_pp p
| TCall(_, c) -> tm_call_pp c
| TIf(_, i) -> tm_if_pp i
| TPanic(_, Some t) -> sprintf "panic %s" (term_pp t)
| TPanic(_, None) -> "panic"
| TError(_) -> "<SyntaxError: Expected Term>"

and ty_annot_pp (TyAnnotation(_, t)) = term_pp t

and tm_if_pp i =
    sprintf "if (%s) %s else %s"
        (term_pp i.tm_if_cond)
        (term_pp i.tm_if_t)
        (term_pp i.tm_if_f)
and tm_call_pp c =
    sprintf "%s(%s)"
        (term_pp c.tm_call_func)
        (tm_call_args_pp c.tm_call_args)

and tm_call_arg_pp arg =
    match arg.tm_call_arg_label with
    | Some(label) ->
        sprintf "%s = %s"
            (ident_pp label)
            (term_pp arg.tm_call_arg_value)
    | None ->
        term_pp arg.tm_call_arg_value

and tm_call_args_pp args =
    Vector.pp tm_call_arg_pp args
and tm_prop_pp { tm_prop_lhs = lhs; tm_prop_rhs = rhs } =
    sprintf "%s.%s" (term_pp lhs) (ident_pp rhs)


and tm_extern_pp e =
    sprintf "extern %s" (ident_pp e)


and tm_l_module_pp m =
    let decls = m.tm_module_decls in
    let decls_str = Vector.pp ~sep:"\n" decl_pp decls in
    sprintf "module {\n%s\n}" decls_str

and opt_ty_annot_pp = function
| Some(a) -> sprintf ": %s" (ty_annot_pp a)
| None -> ""


and binding_ident_pp = function
| BIWildcard(_) -> "_"
| BIName(_, _, ident) -> ident_pp ident

and tm_arrow_param_pp a =
    let annot = a.tm_arrow_param_ty_annotation in
    match a.tm_arrow_param_label with
    | Some(bi) -> sprintf "%s: %s" (binding_ident_pp bi) (ty_annot_pp annot)
    | None -> ty_annot_pp annot

and tm_annotation_pp = function
| Some(t) -> sprintf ": %s" (term_pp t)
| None -> ""

and tm_arrow_pp a =
    let params = a.tm_arrow_params
    |> Vector.pp ~sep:", " tm_arrow_param_pp
    in
    let rhs = a.tm_arrow_rhs in
    sprintf "(%s) -> %s" params (ty_annot_pp rhs)


and decl_pp = function
| DDef(_, d) -> decl_def_pp d
| DError(_) -> "<Syntax error>;"

and params_pp (params: params) = Vector.pp ~sep:", " param_pp params

and param_pp param =
    let ident_s = binding_ident_pp param.param_binding_ident in
    match param.param_ty_annotation with
    | Some(tm) ->
        sprintf "%s: %s" ident_s (ty_annot_pp tm)
    | None ->
        ident_s



and decl_def_pp (d: decl_def) = 
    if (d.decl_def_params |> Vector.length) > 0
    then
        sprintf "def %s(%s)%s = %s;"
            (d.decl_def_name |> binding_ident_pp)
            (d.decl_def_params |> params_pp)
            (d.decl_def_annotation |> opt_ty_annot_pp)
            (d.decl_def_rhs |> term_pp)
    else
        sprintf "def %s%s = %s;"
            (d.decl_def_name |> binding_ident_pp)
            (d.decl_def_annotation |> opt_ty_annot_pp)
            (d.decl_def_rhs |> term_pp)

