open LibCore.Prelude
open Syn

module V = Syn

type t = decl

let meta = function
| DDef(m, _) -> m
| DError(m) -> m


module Def = struct
    type t = decl_def
    let name d = d.decl_def_name

    let annotation d = d.decl_def_annotation

    let rhs d = d.decl_def_rhs

    let params d = d.decl_def_params

    let scope d = d.decl_def_scope

    let make meta ~scope ~name ~annotation ~rhs ~params =
    DDef(meta, {
        decl_def_name = name;
        decl_def_annotation = annotation;
        decl_def_rhs = rhs;
        decl_def_params = params;
        decl_def_scope = scope;
    })

    let pp = decl_def_pp
end

let def = Def.make

let error meta =
    DError(meta)

let binding_name = function
| DDef(_, def) ->
    let binding_ident = Def.name def in
    BindingIdent.label binding_ident
| DError(_) -> None

let pp = decl_pp

include Meta.HasMeta(struct
    type t = decl
    let meta = meta
end)