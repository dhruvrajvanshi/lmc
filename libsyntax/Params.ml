include LibCore
include Prelude

open Syn

type t = params
let pp =
    Vector.pp ~sep:", " Param.pp
