open LibCore
open Prelude

module V = Syn
open Syn
type t = Syn.node

let decl d =
    V.NDecl d

let term t =
    V.NTerm t

let compilation_unit cu =
    V.NCompilationUnit cu

let program p =
    V.NProgram p

let meta = function
| V.NProgram(_) -> panicf "No meta for program"
| V.NTerm(t) -> Term.meta t
| V.NDecl(d) -> Declaration.meta d
| V.NCompilationUnit(cu) -> CompilationUnit.meta cu

let pp = function
| V.NProgram(p) -> Program.pp p
| V.NTerm(t) -> Term.pp t
| V.NDecl(d) -> Declaration.pp d
| V.NCompilationUnit(cu) -> CompilationUnit.pp cu

type children = t Vector.t
let children =
    let units_children (u: Syn.compilation_unit String.Hashtbl.t): children =
        u
        |> Hashtbl.to_alist
        |> List.map ~f:(snd >>> compilation_unit)
        |> List.to_array
    in


    let annot_children (annot: ty_annotation): children =
        [|term (TyAnnotation.term annot)|]
    in

    let opt_annot_children (opt: ty_annotation option): children =
        opt |> Option.to_array |> Vector.concat_map ~f:annot_children
    in
    let param_children (p: param): children = Param.(
        ty_annotation p
        |> opt_annot_children
    ) in

    let decl_children (d: Syn.decl): children = match d with
        | DDef (_, def) -> Declaration.Def.(
            let rhs = rhs def in
            let params = params def in
            let annot = annotation def in
            let params_children = params |> Vector.concat_map ~f:param_children in
            let annot_children = annot |> opt_annot_children in
            [ [|term rhs|]; params_children; annot_children ]
            |> Array.concat
        )

        | DError _ -> [||]
    in

    let arrow_param_children param = Term.Arrow.Param.(
        param |> ty_annotation |> annot_children
    ) in

    let arg_children arg = Term.Call.Arg.(
        let tm = value arg in
        [| term tm |]
    ) in

    let term_children (tm: Syn.term): children = match tm with
        | TLInt (_, _) -> [||]
        | TVar (_, _) -> [||]
        | TArrow (_, arr) -> Term.Arrow.(
            let params = params arr in
            let rhs_children = annot_children (rhs arr) in
            let params_children = params |> Vector.concat_map ~f:arrow_param_children in
            [params_children; rhs_children]
            |> Array.concat
        )
        | TModule (_, m) -> Term.Module.(
            declarations m |> Vector.map ~f:decl
        )
        | TExtern (_, _) -> [||]
        | TProp (_, prop) -> Term.Prop.(
            [| term (lhs prop) |]
        )
        | TCall (_, c) -> Term.Call.(
            let func = func c in
            let args = args c in
            let args_children = args |> Vector.concat_map ~f:arg_children in
            [[|term func|]; args_children]
            |> Array.concat
        )
        | TIf (_, i) -> Term.If.([|
            term (cond i);
            term (true_branch i);
            term (false_branch i);
        |])
        | TPanic (_, Some(tm)) -> [|term tm|]
        | TPanic (_, None) -> [||]
        | TError _ -> [||]
        in

    let sf_children (sf: SourceFile.t): children =
        sf |> SourceFile.declarations |> Vector.map ~f:decl in
    V.(function
    | NProgram(p) ->
        p |> Program.units |> units_children
    | NCompilationUnit(cu) -> CompilationUnit.V.(match cu with
        | CUDirectory(_, dir) -> units_children (CUDir.units dir)
        | CUSourceFile(_, sf) -> sf_children sf
    )
    | NTerm(t) -> term_children t
    | NDecl(d) -> decl_children d
    )

include Meta.HasMeta(struct
    type nonrec t = t
    let meta = meta
end)


