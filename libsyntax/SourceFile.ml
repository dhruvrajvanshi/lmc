open LibCore
open Syn

type t = source_file

let declarations sf = sf.sf_declarations

let self_scope sf = sf.sf_self_scope

let name sf = sf.sf_name

let make ~scope ~declarations ~name = {
    sf_declarations = declarations;
    sf_self_scope = scope;
    sf_name = name
}

let pp sf =
    let decls = declarations sf in
    Vector.pp ~sep:"\n" Declaration.pp decls
