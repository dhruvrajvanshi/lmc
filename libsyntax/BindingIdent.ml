open Syn

module V = Syn


type t = binding_ident


let wildcard meta scope = BIWildcard(meta, scope)
(** A wildcard binding ident that ignores
    the name *)

let name meta scope ident = BIName(meta, scope, ident)
(** A named binding ident that adds ident
    to scope *)


let pp = binding_ident_pp


let meta = function
| BIWildcard(m, _) -> m
| BIName(m, _, _) -> m

let label = function
| BIWildcard(_) -> None
| BIName(_, _, ident) -> Some(Ident.name ident)

let binding_scope = function
| BIWildcard(_, scope) -> scope
| BIName(_, scope, _) -> scope

include Meta.HasMeta(struct
    type t = binding_ident
    let meta = meta
end)
