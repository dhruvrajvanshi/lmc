open LibCore
open Prelude

type t = {
    map: Symbol.t String.Hashtbl.t;
    parent: t option;
}

let parent self = self.parent

let symbols self =
    self.map |> Hashtbl.to_alist |> List.map ~f:snd

let pp self =
    self.map
    |> Hashtbl.to_alist
    |> Vector.of_list
    |> Vector.pp ~sep:",\n" (fun (key, data) ->
        sprintf "\t%s -> %s" key (Symbol.pp data)
    )
    |> sprintf "{\n%s\n}"

let rec resolve self ~name  =
    Hashtbl.find self.map name
    |> Option.(or_else_bind ~f:(fun () ->
        parent self >>= resolve ~name
    ))

let contains self ~name =
    Hashtbl.mem self.map name


let get_local self ~name =
    Hashtbl.find self.map name

let make parent = {
    map = String.Hashtbl.of_alist_exn [];
    parent;
}
let root () = make None
let empty parent = make (Some parent)


let add_binding ~name ~symbol self =
    let tbl = self.map in
    Hashtbl.add_exn tbl ~key:name ~data:symbol
