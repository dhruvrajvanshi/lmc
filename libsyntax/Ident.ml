open Syn

type t = ident
let make meta name =
    { ident_meta = meta; ident_name = name }

let meta i = i.ident_meta
let name i = i.ident_name

let pp = name

include Meta.HasMeta(struct
    type nonrec t = t
    let meta = meta
end)
