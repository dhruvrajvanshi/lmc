open Syn

type t = cu_directory

let units d = d.cu_dir_units
let name d = d.cu_dir_name

let self_scope d = d.cu_dir_self_scope