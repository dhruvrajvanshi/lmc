open LibCore.Prelude
open Syn

type t = program


let make units scope =
    { prog_units = units; prog_scope = scope }

let scope p = p.prog_scope

let units p = p.prog_units

let loc p =
    let start = Pos.make 0 0 in
    let stop = start in
    Loc.make ~path:"program" ~start ~stop

let pp p =
    units p
    |> Hashtbl.to_alist
    |> List.map ~f:(snd >>> CompilationUnit.pp)
    |> List.fold ~init:"" ~f:(sprintf "%s\n%s")


