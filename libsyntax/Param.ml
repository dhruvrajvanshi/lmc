open LibCore.Prelude
open Syn
module V = Syn

type t = Syn.param

let binding_ident t = t.param_binding_ident

let ty_annotation t = t.param_ty_annotation

let loc p =
    let bi_loc = BindingIdent.loc (binding_ident p) in
    let annot_loc = p |> ty_annotation
        |> Option.map ~f:TyAnnotation.loc
        |> Option.value ~default:bi_loc in
    Loc.between bi_loc annot_loc

let make bi annnotation = {
    param_binding_ident = bi;
    param_ty_annotation = annnotation;
}

let pp = param_pp
