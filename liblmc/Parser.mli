
val parse_program
    :  id_gen:IdGen.t
    -> dirs: string list
    -> Syntax.Program.t
