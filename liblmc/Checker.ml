open Prelude
open Syntax
open Syn

module Tm = Term
module Dcl = Declaration
module Def = Dcl.Def
module Sf = SourceFile
module Id = Ident
module Cu = CompilationUnit

module type Primitives = LibCore.Primitives.T

module type TypeMaps = sig
    val symbol_type_map: Type.t Symbol.Hashtbl.t
    val term_type_map: Type.t Int.Hashtbl.t
    val extern_values:  Type.t String.Hashtbl.t
end

module type Result = sig
    include Primitives
    include TypeMaps
    val type_of_term : Term.t -> Type.t
    val type_of_symbol : Symbol.t -> Type.t
end

module MakeResult(P: Primitives) (M: TypeMaps): Result = struct
    include M
    include P

    let type_of_symbol s =
        Hashtbl.find M.symbol_type_map s
        |> Option.or_else ~f:(fun _ -> panicf "Compiler bug: No type for symbol %s" (Symbol.pp s))

    let type_of_term tm =
        match tm with
        | TVar(_, v) ->
            let ident = Tm.Var.ident v in
            let name = ident |> Ident.name in
            let scope = ident |> Ident.scope in
            let sym = Scope.resolve scope ~name
                |> Option.or_else ~f:(fun _ -> panicf "Compiler bug: No symbol in scope") in
            type_of_symbol sym
        | _ ->
        let id = Term.id tm in
        Hashtbl.find term_type_map id
        |> Option.or_else ~f:(fun _ ->
            panicf "Compiler bug: No type for term %i after type checking\n%s"
                id (Term.pp tm)
        )

end


let check_program
    (module Primitives: Primitives.T) (symbol_gen: Symbol.Gen.t) (symbol_table: SymbolTable.t) p =
    let module Value = Type.Make(Primitives) in
    let module Type = Value in

    let values = Symbol.Hashtbl.of_alist_exn Value.[
        unit_val_symbol, t_unit builtin_loc;
        true_symbol, t_bool builtin_loc true;
        false_symbol, t_bool builtin_loc false;
    ] in

    let extern_values = Value.extern_values in

    let _ = begin
        let scope = Program.scope p in
        let n name symbol = Scope.add_binding scope ~name ~symbol in
        n "true" Type.true_symbol;
        n "false" Type.false_symbol;
        n "unit" Type.unit_val_symbol;
    end in 
    let type_map = Symbol.Hashtbl.of_alist_exn [
        Type.unit_val_symbol,  Type.unit_ty;
        Type.true_symbol, Type.bool_ty;
        Type.false_symbol, Type.bool_ty;
    ] in

    let checked_node_set = Int.Hash_set.of_list [] in
    let term_id_type_map = Int.Hashtbl.of_alist_exn [] in
    let get_local_symbol scope name =
        Scope.get_local scope ~name
        |> Option.or_else ~f:(fun _ ->
            panicf "Compiler bug: Unbound name %s" name
        )
    in
    let resolve_symbol scope name =
        Scope.resolve scope ~name
        |> Option.or_else ~f:(fun _ ->
            panicf "Compiler bug: Unbound name %s" name
        )
    in

    let error loc msg =
        printf "%s: %s\n" (Loc.show loc) msg
    in

    let errorf loc fmt = ksprintf (error loc) fmt
    in

    let assign_type sym ty =
        Hashtbl.add_exn ~msg:"assign_type" type_map ~key:sym ~data:ty
    in

    let assign_value sym ty =
        Hashtbl.add_exn ~msg:"assign_value" values ~key:sym ~data:ty
    in
    
    let decl_node_of_symbol sym =
        SymbolTable.decl_node_of_symbol symbol_table sym
        |> Option.or_else ~f:(fun _ ->
            panicf "Compiler bug: No decl node for %s" (Symbol.pp sym)
        )
    in

    let resolve_prop_of_symbol = 
        SymbolTable.resolve_prop_of_symbol symbol_table
    in


    let rec infer_module_scope loc id (scope: Scope.t): Type.t =
        Scope.symbols scope
        |> List.map ~f:(fun sym -> sym, infer_symbol sym)
        |> List.to_array
        |> Type.t_module_type loc

    and value_of_module_scope loc id (scope: Scope.t): Type.t =
        Scope.symbols scope
        |> List.map ~f:(fun sym -> sym, get_symbol_value sym)
        |> List.to_array
        |> Type.t_module loc

    and get_symbol_value (sym: Symbol.t): Value.t =
        match Hashtbl.find values sym with
        | Some v -> v
        | None ->
            let decl_node = decl_node_of_symbol sym in
            check_node decl_node;
            match SymbolTable.param_loc symbol_table sym with
            | Some(l) ->
                let ty = infer_symbol sym in
                Value.t_param_ref l ty sym
            | None ->
                match Hashtbl.find values sym with
                | Some t -> t
                | None ->
                    panicf
                        "Compiler bug: No value for symbol %s"
                        (Symbol.pp sym)

    and get_prop_type_of_symbol sym name =
        match resolve_prop_of_symbol sym name with
        | Some s -> Some(infer_symbol s)
        | None -> None

    and get_prop_value_of_symbol sym name =
        match resolve_prop_of_symbol sym name with
        | Some s -> Some(get_symbol_value s)
        | None -> None

    and check_node node =
        if Hash_set.mem checked_node_set (Node.id node)
        then (
            ()
        )
        else
            ( match node with
            | NTerm _ -> failwith "<case NTerm>"
            | NDecl decl ->
                check_decl decl
            | NCompilationUnit cu ->
                check_compilation_unit cu
            | NProgram _ -> failwith "<case NProgram>"
            )
            ;
            Hash_set.add checked_node_set (Node.id node)

    and t_var_to_value (v: Term.Var.t): Value.t =
        let module V = Tm.Var in
        let ident = V.ident v in
        let name = Id.name ident in
        let scope = Id.scope ident in
        let symbol = resolve_symbol scope name in
        get_symbol_value symbol

    and tm_to_value (loc: Loc.t) (ty: Type.t) = function
        | TLInt (_, i) -> Value.t_int loc i
        | TVar (_, v) -> t_var_to_value v
        | TArrow (_, arrow) ->
            let module A = Tm.Arrow in
            let module P = A.Param in
            let params = A.params arrow in
            let ret = A.rhs arrow in
            let arrow_params = params
                |> Vector.map ~f:(fun p ->
                    
                    let scope = Tm.Arrow.self_scope arrow in
                    let label = P.label p
                        |> Option.concat_map
                            ~f:BindingIdent.label
                        |> Option.map ~f:(get_local_symbol scope) in
                    let ty = P.ty_annotation p |> annotation_to_type in
                    label, ty
                )
                in
            let rhs = annotation_to_type ret in
            Value.arrow loc arrow_params rhs
        | TModule (meta, m) ->
            Tm.Module.self_scope m
            |> Scope.symbols
            |> List.map ~f:(fun sym -> sym, get_symbol_value sym)
            |> List.to_array
            |> Value.t_module loc
        | TExtern (_, ident) ->
            let name = Ident.name ident in
            (match Hashtbl.find extern_values name with
            | Some(t) -> t
            | None ->
                Value.t_extern loc ty (Ident.name ident)
            )
        | TProp (_, prop) ->
            prop_to_value prop
        | TCall (_, c) ->
            failwith "<case TCall>"
        | TIf (_, _) -> failwith "<case TIf>"
        | TPanic (_, p) ->
            let module P = Tm.Panic in
            let tm_opt = P.term p in
            let value = tm_opt
                |> Option.map ~f:(fun tm ->
                    let ty = infer_term tm in
                    tm_to_value loc ty tm
                )
                |> Option.value ~default:Value.(t_unit builtin_loc) in
                
            Type.t_panic loc ty value
        | TError _ -> Value.t_error loc

    and check_source_file meta sym sf =
        infer_source_file meta sym sf |> ignore

    and infer_dir id loc sym dir =
        dir |> CUDir.units |> Hashtbl.to_alist |>
            List.iter ~f:(snd >>> check_compilation_unit);
        let id = Symbol.id sym in
        let scope = CUDir.self_scope dir in
        let ty = infer_module_scope loc id scope in
        let value = value_of_module_scope loc id scope in
        assign_type sym ty;
        assign_value sym value;
        ty

    and check_compilation_unit cu =
        let name = Cu.name cu in
        let parent_scope = Cu.scope cu in
        let symbol = get_local_symbol parent_scope name in
        (match cu with
        | CUDirectory(_, dir) ->
            let loc = Cu.loc cu in
            let id = Cu.id cu in
            infer_dir id loc symbol dir |> ignore
        | CUSourceFile(m, sf) ->
            check_source_file m symbol sf
        )

    and check_decl d =

        if Hash_set.mem checked_node_set (Declaration.id d)
        then ()
        else
            match d with
            | DDef (meta, def) -> check_def meta def
            | DError _ -> ()

    and type_eq t1 t2: bool =
        Type.(match t1, t2 with
        | TConstr(_, s1), TConstr(_, s2) ->
            (Symbol.id s1) = (Symbol.id s2)
        | _ -> false
        )

    and check_term tm ty =
        let id = Tm.id tm in
        let add_typ () = 
            Hashtbl.add_exn
                ~msg:"check_term"
                term_id_type_map
                ~key:id
                ~data:ty in
        match tm with
        | TExtern _ ->
            add_typ()
        | TIf (meta, i) ->
            check_t_if meta i ty;
            add_typ()
        | TPanic(_, tm_opt) ->
            tm_opt |> Option.iter ~f:(infer_term >>> ignore);
            add_typ()
        | _ ->
            let tm_ty = infer_term tm in
            if not (type_eq tm_ty ty)
            then
                errorf (Tm.loc tm) "Type mismatch: Expected %s, Found %s"
                    (Type.pp ty)
                    (Type.pp tm_ty)
        ;

    and check_def meta def =
        let rhs = Def.rhs def in
        let binding_ident = Def.name def in
        let params = Def.params def in
        let annot_opt = Def.annotation def in
        let arrow_params = infer_params params in
        let rhs_ty = match annot_opt with

        | None ->
            infer_term rhs
        | Some annot ->
            check_annotation annot;
            let ty = annotation_to_type annot in
            check_term rhs ty;
            ty
        in
        let loc = Meta.loc meta in
        let ty, value = if
            Meta.has_flag meta MetaFlags.has_param_list
        then
            (* TODO: Assign value of ident to a closure *)
            Type.arrow loc arrow_params rhs_ty, Value.t_error loc
        else (
            let rhs_value = tm_to_value loc rhs_ty rhs in
            rhs_ty, rhs_value
        )
        in
        bind_binding_ident binding_ident ~ty ~value;
    
    and infer_params params =
        Vector.map params ~f:infer_param

    and infer_param param =
        let bi = Param.binding_ident param in
        let annot_opt = Param.ty_annotation param in
        Option.iter annot_opt ~f:check_annotation;
        if annot_opt |> Option.is_none
        then
            errorf (BindingIdent.loc bi) "Missing type annotation";
        let ty = Option.map
            annot_opt ~f:annotation_to_type
            |> Option.value
                ~default:(Type.t_error (BindingIdent.loc bi)) in
        bind_binding_ident_without_value bi ~ty;
        let label_opt = BindingIdent.label bi in
        let scope = BindingIdent.binding_scope bi in
        let sym_opt = label_opt |>
            Option.map ~f:(get_local_symbol scope) in
        sym_opt, ty

    and annotation_to_type annot =
        let tm = TyAnnotation.term annot in
        tm_to_value (Tm.loc tm) Type.type_ty tm


    and find_prop bindings name =
        bindings
        |> Array.find
            ~f:(fst >>> Symbol.name >>> String.equal name)

    and prop_to_value prop =
        let module P = Tm.Prop in
        let lhs = P.lhs prop in
        let rhs = P.rhs prop in
        let lhs_loc = Tm.loc lhs in
        let rhs_loc = Ident.loc rhs in
        let rhs_name = Ident.name rhs in
        let from_value () =
            let lhs_type = infer_term lhs in
            let lhs_value = tm_to_value lhs_loc lhs_type lhs in
            Value.(match lhs_value with
            | TModule(_, _, bindings) ->
                rhs_name
                |> find_prop bindings
                |> Option.map ~f:snd
                |> Option.value ~default:(Value.t_error rhs_loc)
            | _ -> Value.t_error lhs_loc
            )
        in
        match lhs with
        | TVar(_, i) when var_has_associated_prop i rhs_name ->
            let scope = Tm.scope lhs in
            let var_ident = Tm.Var.ident i in
            let var_name = Ident.name var_ident in
            let symbol = resolve_symbol scope var_name in
            ( match get_prop_value_of_symbol symbol rhs_name with
            | Some t -> t
            | None ->
                panicf "prop_to_value"
            )
        | _ ->
            from_value ()

    and var_has_associated_prop v name =
        let ident = Tm.Var.ident v in
        let scope = Ident.scope ident in
        let var_name = Ident.name ident in
        let symbol = resolve_symbol scope var_name in
        SymbolTable.resolve_prop_of_symbol symbol_table symbol name
        |> Option.is_some

    and check_annotation annot =
        let tm = TyAnnotation.term annot in
        check_term tm Type.type_ty

    and bind_binding_ident bi ~ty ~value =
        bind_binding_ident_without_value bi ~ty;
        match bi with
        | BIWildcard _ -> ()
        | BIName (_, scope, ident) ->
            let name = Id.name ident in
            let symbol = get_local_symbol scope name in
            assign_value symbol value
        
    and bind_binding_ident_without_value bi ~ty =
        match bi with
        | BIWildcard (_, _) -> failwith "<case>"
        | BIName (_, scope, ident) ->
            let name = Id.name ident in
            let symbol = get_local_symbol scope name in
            assign_type symbol ty;

    and infer_term tm =
        let id = Tm.id tm in
        match Hashtbl.find term_id_type_map id with
        | Some t ->
            t
        | None ->
            let loc = Term.loc tm in
            let ty = match tm with
                | TLInt (_, _) -> Type.int_ty
                | TVar (meta, t_var) -> infer_t_var meta t_var
                | TArrow (meta, arrow) -> infer_t_arrow meta arrow
                | TModule (meta, m) -> infer_t_module meta m
                | TExtern (_, _) -> failwith "<case TExtern>"
                | TProp (meta, prop) ->
                    infer_t_prop meta prop
                | TCall (meta, call) ->
                    infer_t_call meta call
                | TIf (meta, i) -> infer_t_if meta i
                | TPanic (_, _) -> failwith "<case TPanic>"
                | TError _ -> Type.t_error loc
            in
            Hashtbl.add_exn
                ~msg:"infer_term"
                term_id_type_map
                ~key:id ~data:ty;

            ty

    and infer_t_if meta i =
        let module I = Tm.If in
        let c = I.cond i in
        check_term c Type.bool_ty;
        let t = I.true_branch i in
        let ty = infer_term t in
        let f = I.false_branch i in
        check_term f ty;
        ty

    and check_t_if meta i ty = 
        let module I = Tm.If in

        let c = I.cond i in
        check_term c Type.bool_ty;
        let t = I.true_branch i in
        check_term t ty;
        let f = I.false_branch i in

        check_term f ty

    and infer_t_call meta call =
        let module C = Tm.Call in
        let func = C.func call in
        let func_loc = Tm.loc func in
        let fn_ty = infer_term func in
        Type.(match fn_ty with
        | TArrow(_, params, ret) ->
            check_func_args func_loc params (C.args call);
            ret
        | _ ->
            todo "not a function"
        )

    and _assert_unlabeled_args args =
        if Vector.exists args ~f:(Tm.Call.Arg.label >>> Option.is_some)
        then todo "Labeled args"

    and check_func_args fn_loc params args =
        let params_len = Vector.length params in
        let args_len = Vector.length args in
        let arr_len = Int.max params_len args_len in
        _assert_unlabeled_args args;
        Vector.create ~len:arr_len 0
        |> Vector.iteri ~f:(fun i _ ->
            if i < params_len && i < args_len
            then
                let (_, ty) = params.(i) in
                let arg = args.(i) in
                let tm = Tm.Call.Arg.value arg in
                check_term tm ty;
            else
                todo "Args length mismatch"
        )

    and infer_t_arrow meta arrow =
        let module A = Tm.Arrow in
        let module P = A.Param in
        let infer_arrow_param param =
            let bi_opt = P.label param in
            let annot = P.ty_annotation param in
            check_annotation annot;
            let ty = annotation_to_type annot in
            bi_opt |> Option.iter ~f:(bind_binding_ident_without_value ~ty)
        in
        let params = A.params arrow in
        params |> Vector.iter ~f:infer_arrow_param;
        let rhs = A.rhs arrow in
        check_annotation rhs;
        Type.type_ty
    
    and infer_t_module meta m =
        let module M = Term.Module in
        let scope = M.self_scope m in
        let loc = Meta.loc meta in
        infer_module_scope loc (Meta.id meta) scope

    and infer_t_var meta t_var =
        let ident = Tm.Var.ident t_var in
        infer_ident ident

    and infer_ident ident =
        let name = Id.name ident in
        let scope = Id.scope ident in
        let symbol = resolve_symbol scope name in
        infer_symbol symbol

    and infer_t_prop meta prop =
        let module P = Term.Prop in
        let lhs = P.lhs prop in
        let rhs = P.rhs prop in
        let prop_name = Id.name rhs in
        let loc = Meta.loc meta in
        let rhs_loc = Ident.loc rhs in
        let from_type () =
            let lhs_ty = infer_term lhs in
            ( match lhs_ty with
            | Type.TModuleType(_, bindings) ->
                find_prop bindings prop_name
                |> Option.map ~f:snd
                |> Option.or_else ~f:(fun _ ->
                    errorf loc "No such prop %s" (Id.pp rhs);
                    Type.t_error rhs_loc
                )
            | _ ->
                errorf loc "No such prop %s" (Id.pp rhs);
                Type.t_error rhs_loc
            )
        in
        match lhs with
        | TVar(_, v) ->
            let scope = Tm.scope lhs in
            let var_ident = Tm.Var.ident v in
            let var_name = Ident.name var_ident in
            let symbol = resolve_symbol scope var_name in
            ( match get_prop_type_of_symbol symbol prop_name with
            | Some t -> t
            | None ->
                from_type ()
            )
        | _ ->
            from_type ()

    and infer_symbol sym =
        match Hashtbl.find type_map sym with
        | None -> 
            let node = decl_node_of_symbol sym in
            check_node node;
            Hashtbl.find_exn type_map sym
        | Some(t) ->
            t
    and infer_source_file meta sym sf: Type.t =
        Hashtbl.find_or_add type_map sym ~default:(fun _ ->
            sf |> Sf.declarations |> Vector.iter ~f:check_decl;
            let scope = Sf.self_scope sf in
            let id = Symbol.id sym in
            let loc = Meta.loc meta in
            let ty = infer_module_scope loc id scope in
            let value = value_of_module_scope loc id scope in
            assign_type sym ty;
            assign_value sym value;
            ty
        )
        
    in
    let rec infer_unit u: Type.t =
        
        let scope = Cu.scope u in
        let name = Cu.name u in
        let symbol = get_local_symbol scope name in
        Hashtbl.find_or_add type_map symbol ~default:(fun _ ->
            match Hashtbl.find type_map symbol with
            | Some t -> t
            | None ->
                let t = CompilationUnit.V.(match u with
                | CUSourceFile(meta, sf) -> infer_source_file meta symbol sf
                | CUDirectory(m, dir) ->
                    let loc = Cu.loc u in
                    let id = Symbol.id symbol in
                    infer_dir id loc symbol dir
                ) in
                t
        )

    and infer_units loc scope id units =
        String.Hashtbl.to_alist units
        |> List.to_array
        |> Array.map ~f:(fun (name, unit) ->
            let sym = get_local_symbol scope name in
            (sym, infer_unit unit)
        )
        |> Type.t_module_type loc
    in
    let check_program p: unit =
        let units = Program.units p in
        let scope = Program.scope p in
        let path = "" in
        let start = Pos.make 0 0 in
        let stop = Pos.make 0 0 in
        let loc = Loc.make ~path ~start ~stop in
        infer_units loc scope 0 units |> ignore;
        ()
    in
    check_program p;
    let module Result = MakeResult(Type)(struct
        let symbol_type_map = type_map
        let term_type_map = term_id_type_map
        let extern_values = extern_values
    end)
    in
    (module Result: Result)
        
