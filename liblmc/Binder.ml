open Prelude
open Syntax

let bind_program ~symbol_gen program  =
    let queue = Queue.create () in
    let parent_map = Int.Hashtbl.of_alist_exn [] in
    let def_node_map = Symbol.Hashtbl.of_alist_exn [] in
    let associated_scope_map = Symbol.Hashtbl.of_alist_exn [] in
    let param_symbols = Symbol.Hashtbl.of_alist_exn [] in
    let global_symbols = Symbol.Hash_set.create () in

    let associate_symbol_scope (symbol: Symbol.t) (scope: Scope.t) =
        Symbol.Hashtbl.add_exn associated_scope_map ~key:symbol ~data:scope
    in
    let set_parent ~(parent: Node.t) (meta: Meta.t) =
        let id = Meta.id meta in
        Hashtbl.add_exn parent_map ~key:id ~data:parent
    in

    let get_parent (meta: Meta.t) =
        let id = Meta.id meta in
        Hashtbl.find_exn parent_map id
    in

    let rec is_global_scoped node =
        Node.V.(match node with
            | NCompilationUnit _ -> true
            | NDecl(d) ->
                get_parent (Declaration.meta d)
                |> is_global_scoped
            | NTerm(Syn.TModule(_)) ->
                let parent = get_parent (Node.meta node) in
                let is_parent_param_less_def =
                    match parent with
                    | NDecl(DDef(meta, _)) ->
                        not (Meta.has_flag meta MetaFlags.has_param_list)
                    | _ ->
                        false
                in
                is_parent_param_less_def && is_global_scoped parent
            | _ -> false
            )
    in



    let mark_as_global s =
        Hash_set.add global_symbols s
    in

    let set_def_node symbol node =
        def_node_map |> Hashtbl.add_exn ~key:symbol ~data:node;
    in

    let add_name_to_scope (scope: Scope.t) (name: string) =
        let symbol = Symbol.Gen.make_symbol symbol_gen name in
        Scope.add_binding scope ~name ~symbol;
        symbol
    in

    let get_local (scope: Scope.t) (name: String.t): Symbol.t =
        Scope.get_local scope ~name
        |> Option.or_else ~f:(fun _ ->
            panicf "Compiler bug: Unbound name %s" name
        )
    in

    let mark_as_param loc s = Hashtbl.add_exn param_symbols ~key:s ~data:loc in

    let bind_binding_ident parent binding_ident = (
        set_parent ~parent (BindingIdent.meta binding_ident);
        binding_ident |> BindingIdent.label
        |> Option.map ~f:(fun name ->
            let scope = BindingIdent.binding_scope binding_ident in
            let symbol = add_name_to_scope scope name in
            set_def_node symbol parent;
            symbol
        )
    )
    in

    let global_module_scopes = Queue.create () in
    let add_global_scope scope =
        Queue.push scope global_module_scopes in

    let bind_compilation_unit parent cu = CompilationUnit.(
        let name = name cu in
        let scope = scope cu in
        let symbol = add_name_to_scope scope name in
        let cu_scope = self_scope cu in
        add_global_scope cu_scope;
        associate_symbol_scope symbol cu_scope;
        set_def_node symbol (Node.compilation_unit cu)
    )
    in

    let bind_decl parent decl =
        let is_global_scoped = is_global_scoped parent in
        let is_rhs_global_scoped = not (Declaration.has_flag decl MetaFlags.has_param_list)
        in
        Declaration.V.(match decl with
        | DDef(_, def) -> Declaration.Def.(
            let params = params def in
            params |> Vector.iter ~f:(fun param ->
                param
                |> Param.binding_ident
                |> bind_binding_ident (Node.decl decl)
                |> ignore
            );
            params
            |> Vector.iter ~f:(fun p ->
                let bi = Param.binding_ident p in
                let label = BindingIdent.label bi in
                let scope = BindingIdent.binding_scope bi in
                let loc = BindingIdent.loc bi in
                label
                |> Option.iter ~f:(get_local scope >>> mark_as_param loc)
            );
            let bi_symbol_opt = def
                |> name
                |> bind_binding_ident (Node.decl decl) in
            match rhs def with
            | TModule(meta, m) when not (Declaration.has_flag decl MetaFlags.has_param_list) ->
                bi_symbol_opt
                |> Option.iter ~f:(fun bi_symbol ->
                    let mod_scope = Term.Module.self_scope m in
                    associate_symbol_scope bi_symbol mod_scope;

                    if is_global_scoped && is_rhs_global_scoped
                    then
                        add_global_scope mod_scope
                )

            | _ -> ()


    )
    | DError(_) -> ()
    )
    in

    let bind_arrow arrow =
        let module P = Term.Arrow.Param in
        let params = Term.Arrow.params arrow in
        let scope = Term.Arrow.self_scope arrow in
        params
        |> Vector.iter ~f:(fun p ->
            let loc = P.loc p in
            p
            |> P.label
            |> Option.concat_map ~f:BindingIdent.label
            |> Option.map ~f:(get_local scope)
            |> Option.iter ~f:(mark_as_param loc)
        )
    in

    let bind_term parent term = Term.V.(
        match term with
        | TArrow(_, arrow) -> bind_arrow arrow
        | _ -> ()
    )
    in

    let bind_node parent node: unit = Node.(
        children node |> Vector.iter ~f:(fun n ->
            set_parent ~parent:node (meta n);
            queue |> Queue.add (node, n);
        );
        V.(match node with
        | NCompilationUnit(cu) ->
            bind_compilation_unit parent cu
        | NDecl(d) -> bind_decl parent d
        | NTerm(t) ->
            bind_term parent t
        | NProgram(_) -> panicf "Compiler bug: Unexpected program in add_node"
        ))
    in


    let rec step () =
        if Queue.is_empty queue then
            ()
        else
            let (parent, node) = Queue.take queue in
            bind_node parent node;
            step ()
    in
    program
    |> Program.units
    |> String.Hashtbl.iter ~f:(fun unit ->
        queue |> Queue.add (Node.program program, Node.compilation_unit unit)
    );
    step ();
    Queue.iter (Scope.symbols >>> List.iter ~f:mark_as_global) global_module_scopes ;
    SymbolTable.make
        ~parent_map
        ~associated_scope_map
        ~def_node_map
        ~param_symbols
        ~global_symbols

    
