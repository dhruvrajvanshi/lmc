
open Prelude


module Token = Syntax.Token

let keyword_tokens = Hashtbl.of_alist_exn (module String) Token.[
    "_", UNDERSCORE;
    "def", DEF;
    "module", MODULE;
    "extern", EXTERN;
    "if", IF;
    "else", ELSE;
    "panic", PANIC;
]

let single_char_tokens = Hashtbl.of_alist_exn (module Char) Token.[
    '(', LPAREN;
    ')', RPAREN;
    ':', COLON;
    ';', SEMICOLON;
    '.', DOT;
    ',', COMMA;
    '{', LBRACE;
    '}', RBRACE;
]

let operator_tokens = Hashtbl.of_alist_exn (module String) Token.[
    "==", EQEQ;
    "=", EQ;
    "->", ARROW;
]

let operator_chars = Hashtbl.keys operator_tokens
    |> List.concat_map ~f:(String.to_list)
    |> Hash_set.of_list (module Char)

type t = {
    input: string;
    path: Path.t;
    id_gen: IdGen.t;
    mutable _start_index: int;
    mutable _current_index: int;
    mutable _start_line: int;
    mutable _start_col: int;
    mutable _current_line: int;
    mutable _current_col: int;
}

let dummy_scope = Syntax.Scope.root ()

let make ~id_gen ~input ~(path): t =
    let _start_line = 1 in
    let _start_col = 1 in
    let _current_line = 1 in
    let _current_col = 1 in
    let _start_index = 0 in
    let _current_index = 0 in
    {
        id_gen;
        input; _start_line; _start_col; _current_line;
        _current_col; _start_index; _current_index; path;
    }

let eof_reached self =
    self._current_index >= String.length self.input

let eof_char = Char.of_int_exn 0

let current_char self =
    if eof_reached self
    then eof_char
    else self.input.[self._current_index]

let start_token self =
    self._start_index <- self._current_index;
    self._start_line <- self._current_line;
    self._start_col <- self._current_col

let lexeme self =
    let inp = self.input in
    String.slice inp self._start_index self._current_index

let make_token_with_lexeme self variant lexeme =
    let open Syntax in
    let id = IdGen.next_id self.id_gen in
    let path = self.path in
    let start = Pos.make self._start_line self._start_col in
    let stop = Pos.make self._current_line self._current_col in
    let loc = Loc.make ~path ~start ~stop in
    let meta = Meta.make ~id ~loc ~scope:dummy_scope ~flags:MetaFlags.empty in
    Token.make ~meta ~variant ~lexeme

let make_token self variant =
    make_token_with_lexeme self variant (lexeme self)

let advance self =
    if eof_reached self
    then ()
    else begin
        let c = current_char self in
        self._current_index <- self._current_index + 1;
        if Char.equal c '\n'
        then begin
            self._current_line <- self._current_line + 1;
            self._current_col <- 1
        end
        else
            self._current_col <- self._current_col + 1
    end

let rec skip_whitespace self: unit =
    let c = current_char self in
    if not (eof_reached self) && Char.is_whitespace c
    then begin
        advance self |> ignore;
        (skip_whitespace [@tailcall]) self
    end
    else
        ()

let is_ident_or_keyword_starter c =
    Char.equal c '_' || Char.is_alpha c

let is_ident_or_keyword_char c =
    is_ident_or_keyword_starter c || Char.is_digit c

let ident_or_keyword self =
    let rec go () =
        if is_ident_or_keyword_char (current_char self)
        then (advance self; go())
        else ()
    in
    go();
    let lexeme = lexeme self in
    let variant =
        Hashtbl.find keyword_tokens lexeme
        |> Option.value ~default:Token.ID
        in
    make_token_with_lexeme self variant lexeme

let operator self =
    let rec go () =
        let c = current_char self in
        if Hash_set.mem operator_chars c
        then begin
            advance self;
            (go [@tailcall]) ()
        end
        else
            lexeme self
    in
    let lexeme = go () in
    match Hashtbl.find operator_tokens lexeme with
    | Some(v) ->
        make_token_with_lexeme self v lexeme
    | None ->
        panicf "%s(%i:%i): Invalid operator %s"
            self.path
            self._current_line
            self._current_col
            lexeme

let number self =
    let rec go () =
        if Char.is_digit (current_char self)
        then begin
            advance self;
            (go [@tailcall]) ()
        end
        else
            lexeme self
    in
    let lexeme = go() in
    make_token_with_lexeme self Token.INT lexeme

let next_token self: Token.t =
    skip_whitespace self;
    start_token self;
    match current_char self with
    | c when is_ident_or_keyword_starter c ->
        ident_or_keyword self
    | c when Hashtbl.mem single_char_tokens c ->
        advance self;
        make_token self (Hashtbl.find_exn single_char_tokens c)
    | c when Hash_set.mem operator_chars c ->
        operator self
    | c when Char.is_digit c ->
        number self
    | c when Char.equal c eof_char ->
        make_token_with_lexeme self Token.EOF ""
    | c -> panicf "%s (%i:%i): Unexpected char '%c'"
        self.path
        self._current_line
        self._current_col
        c
