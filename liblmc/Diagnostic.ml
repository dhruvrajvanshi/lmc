module Variant = struct
    type t = UnexpectedChar of char
end
type t = {
    loc: Syntax.Loc.t;
    variant: Variant.t;
}

let make ~loc variant = { loc; variant }
