open Prelude
open Syntax
open Syn

module Dcl = Declaration
module Tm = Term
module Def = Dcl.Def

let compile ~dirs ~out_dir =
    let id_gen = IdGen.make() in
    let symbol_gen = Syntax.Symbol.Gen.make () in
    let module Primitives = LibCore.Primitives.Make(struct
        let next_symbol = Symbol.Gen.make_symbol symbol_gen
    end) in
    let program = Parser.parse_program ~id_gen ~dirs in
    let symbol_table = Binder.bind_program ~symbol_gen program in
    let checker_result = Checker.check_program
            (module Primitives)
            symbol_gen
            symbol_table
            program
    in
    let module Result = (val checker_result) in
    let module IR = LibIR.IR.Make(Result) in

    let value_to_ir (value: Type.t): IR.ty =
        value
    in
    let type_of_term =
        Result.type_of_term >>> value_to_ir
    in


    let rec compile_unit (u: CompilationUnit.t): IR.term =
        let name = CompilationUnit.name u in
        let parent_scope = CompilationUnit.scope u in
        let loc = CompilationUnit.loc u in
        let symbol = get_local parent_scope name in
        match u with
        | CUSourceFile(_, sf) ->
            compile_source_file symbol loc sf
        | CUDirectory(_, dir) ->
            compile_dir symbol loc dir

    and compile_source_file symbol loc sf: IR.term =
        let id = Symbol.id symbol in
        sf
        |> SourceFile.declarations
        |> compile_declarations loc id

    and compile_dir symbol loc dir =
        let id = Symbol.id symbol in
        let scope = CUDir.self_scope dir in
        let units = CUDir.units dir in
        let name = CUDir.name dir in
        compile_units loc scope name id units

    and compile_declarations loc id d =
        d
        |> Vector.concat_map ~f:compile_decl
        |> make_module loc id

    and compile_decl decl: (Symbol.t * IR.term) Vector.t =
        match decl with
        | DDef(meta, def) ->
            compile_def meta def
        | DError(_) -> panicf "can't compile error node"


    and get_local scope name =
        Scope.get_local scope ~name
        |> Option.or_else ~f:(fun _ -> panicf "Compiler bug: No symbol for %s" name)

    and compile_def meta def =
        let params = Def.params def
            |> Vector.map ~f:(Param.binding_ident >>> fun bi ->
                let name = BindingIdent.label bi in
                let scope = BindingIdent.binding_scope bi in
                name |> Option.map ~f:(get_local scope)
            ) in
        let bi = Def.name def in
        let scope = BindingIdent.binding_scope bi in
        let name_opt = BindingIdent.label bi in
        match name_opt with
        | None -> todo "compile_def for unlabeled names unimplemented"
        | Some(name) ->
            let symbol = get_local scope name in
            let ty = Result.type_of_symbol symbol
                |> value_to_ir in
            let tm = if Meta.has_flag meta MetaFlags.has_param_list
                then
                    let tm = compile_term (Def.rhs def) in
                    let loc = Meta.loc meta in
                    let tm = IR.t_lambda loc ty params tm in
                    tm
                else (
                    let tm = compile_term (Def.rhs def) in
                    tm
                ) in
            [|symbol, tm|]

    and get_ident_symbol i =
        let name = Ident.name i in
        let scope = Ident.scope i in
        Scope.resolve scope ~name |> Option.or_else ~f:(fun _ ->
            panicf "Compiler bug: Unbound ident %s" (Ident.pp i))

    and compile_term tm: IR.term =
        let tm_type = type_of_term tm in
        let meta = Term.meta tm in
        let loc = Meta.loc meta in
        match tm with
            | TLInt (_, i) ->
                IR.t_int loc i
            | TVar (_, v) ->
                let ident = Tm.Var.ident v in
                let sym = get_ident_symbol ident in
                let ty = type_of_term tm in
                IR.t_var loc ty sym
            | TArrow (_, _) -> failwith "<case TArrow>"
            | TModule (meta, m) -> compile_t_module meta m
            | TExtern (_, ident) ->
                let name = Ident.name ident in
                IR.t_extern loc tm_type name
            | TProp (_, p) -> compile_t_prop meta p
            | TCall (_, c) -> compile_t_call meta c tm_type
            | TIf (_, i) -> compile_t_if meta i tm_type
            | TPanic (_, tm_opt) ->
                let tm = tm_opt
                    |> Option.map ~f:compile_term
                    |> Option.value ~default:IR.(t_unit builtin_loc) in
                IR.t_panic loc tm_type tm
            | TError _ -> panicf "tried to compile errors"

    and compile_t_call meta c ty =
        let module C = Tm.Call in
        let f = C.func c |> compile_term in
        let args = C.args c |> Vector.map ~f:(fun arg ->
            let module A = C.Arg in
            ensuref (A.label arg |> Option.is_none)
                "Unimplemented labeled args";
            A.value arg |> compile_term
        ) in
        let loc = Meta.loc meta in
        IR.t_call loc ty f args

    and compile_t_if meta i ty =
        let module I = Tm.If in
        let c = I.cond i |> compile_term in
        let t = I.true_branch i |> compile_term in
        let f = I.false_branch i |> compile_term in
        let loc = Meta.loc meta in
        IR.t_if loc ty c t f

    and compile_t_module meta m =
        let id = Meta.id meta in
        m
        |> Term.Module.declarations
        |> compile_declarations (Meta.loc meta) id

    and compile_t_prop meta p: IR.term =
        let lhs = Tm.Prop.lhs p in
        let rhs = Tm.Prop.rhs p in
        let lhs_ty = type_of_term lhs in
        let name = Ident.name rhs in
        let loc = Meta.loc meta in
        match lhs_ty with
        | IR.TModuleType(_, members) ->
            let compiled_lhs = compile_term lhs in
            let (symbol, rhs_ty) = IR.find_member_by_name members name
                |> Option.unwrap in
            IR.t_prop loc rhs_ty compiled_lhs symbol
        | _ -> panicf "Compiler bug: Checked prop lhs doesn't have module type"

    and compile_units loc scope name id units =
        units
        |> Hashtbl.to_alist
        |> List.to_array
        |> Array.map  ~f:(fun (name, unit) ->
            let symbol = Scope.get_local scope ~name
                |> Option.unwrap in
            let rhs = compile_unit unit in
            symbol, rhs
        )
        |> make_module loc id
    and make_module loc id defs =
        IR.t_module loc defs
    and make_symbol name =
        Symbol.Gen.make_symbol symbol_gen name
    in
    let ir_term =
        program
        |> Program.units
        |> compile_units
            (Program.loc program)
            (Program.scope program) "global_module" 0
        |> ModInit.make (module Primitives) make_symbol
    in

    LibCodegen.JS.codegen (module Result)
        ir_term out_dir;

