open LibCore
open Prelude
open LibIR.IR
 
(*
    a module might have recursive definitions
    so we need to compile them to lazy vals

    example:

    def X = module {
        def a = b;
        def b = 4;
    }
    should be compiled to the IR

    let X =
        let $X_thunks =
            let $a_thunk = lazy (force $b_thunk) in
            let $b_thunk = lazy 4 in
            module {
                a = $a_thunk;
                b = $b_thunk;
            }
        in
        module {
            a = force $X_thunks.a;
            b = force $X_thunks.b;
        }
*)    


let make (module Primitives: Primitives.T)
    (make_symbol: string -> Symbol.t)
    (root: LibIR.IR.term) =
    let module IR = LibIR.IR.Make(Primitives) in


    let rec vars in_symbols hash_set (tm: IR.term): unit =
        let build = vars in_symbols hash_set in

        let build_defs defs = Vector.iter defs ~f:(fun (s, rhs) ->
            build rhs
        ) in
        IR.(match tm with
        | TAlloc _
        | TInt _
        | TBool _
        | TUnit _
        | TParamRef _
        | TConstr _
        | TExtern _
        | TError _ -> ()
        | TVar(_, _, s) when Hash_set.mem in_symbols s -> Hash_set.add hash_set s
        | TVar(_, _, _) -> ()
        | TAssign(_, _, rhs) ->
            panicf "T_Assign"
        | TCall (_, _, f, args) ->
            build f;
            args |> Vector.iter ~f:build
        | TModule (_, _, defs) -> build_defs defs
        | TModuleType (_, defs) -> build_defs defs

        | TProp (_, _, _, rhs) when Hash_set.mem in_symbols rhs  ->
            Hash_set.add hash_set rhs
        | TProp (_, _, t, _) ->
            build t
        | TLambda (_, _, _, b) -> build b
        | TArrow (_, ps, b) ->
            ps |> Array.iter ~f:(snd >>> build);
            build b
        | TPanic (_, _, t) -> build t
        | TIf (_, _, c, t, f) ->
            let b = build in
            b c;
            b t;
            b f;
        | TLazy (_, f) ->
            build f
        | TForce (_, _, t) -> build t
        | TLet (_, bs, t) ->
            bs |> Array.iter ~f:(snd >>> build);
            build t
        )
        
    in
    let __ids = ref 0 in
    let next_mod_id () =
        let id = !__ids in
        __ids := id + 1;
        id
    in
    let rec go ?(name=None) (t: term) = IR.(match t with
        | TVar _
        | TInt _
        | TBool _
        | TError _
        | TConstr _
        | TExtern _
        | TParamRef _
        | TAlloc _
        | TUnit _ -> t
        | TCall (l, ty, f, args) ->
            TCall(l, ty, go f, args |> Array.map ~f:go)
        | TAssign(l, path, t) ->
            TAssign(l, path, go t)
        | TModule (loc, ty, defs) ->
            go_module name loc ty defs
        | TModuleType (loc, defs) -> 
            panicf "<case ModuleType>"
        | TProp (l, ty, t, s) ->
            TProp(l, ty, go t, s)
        | TLambda (l, ty, ps, b) ->
            TLambda(l, ty, ps, go b)
        | TArrow (_, _, _) -> failwith "<case TArrow>"
        | TPanic (l, ty, t) -> TPanic(l, ty, go t)
        | TIf (l, ty, c, t, f) ->
            TIf(l, ty, go c, go t, go f)
        | TLazy (l, t) ->
            TLazy(l, go t)
        | TForce (l, ty, t) ->
            TForce(l, ty, go t)
        | TLet (l, bs, b) ->
            let bs = bs |> Array.map ~f:(fun (s, t) -> s, go ~name:(Some s) t) in
            TLet(l, bs, go b)
    )

    and apply_subst s (t: IR.term) =
        let go = apply_subst s in
        let get = Hashtbl.find s in
        let has = get >>> Option.is_some in
        let go_arr = Array.map ~f:go in
        let go_snd = Array.map ~f:(fun (x, y) -> x, go y) in
        match t with
        | TVar (_, _, sy) when has sy ->
            get sy |> Option.unwrap
        | TProp (_, _, _, sy) when has sy ->
            get sy |> Option.unwrap
        | TProp(l, ty, t, rhs) ->
            TProp(l, ty, go t, rhs)
        | TCall (l, ty, f, args) ->
            TCall(l, ty, go f, go_arr args)
        | TModule (l, n, defs) ->
            TModule(l, n, go_snd defs)
        | TModuleType (l, defs) ->
            TModuleType(l, go_snd defs)
        | TLambda (l, ty, ps, b) ->
            TLambda(l, ty, ps, go b)
        | TArrow (_, _, _) -> failwith "<case>"
        | TPanic (l, ty, t) ->
            TPanic(l, ty, go t)
        | TIf (l, ty, c, t, f) ->
            TIf(l, ty, go c, go t, go f)
        | TLazy (_, _) -> failwith "<case>"
        | TForce (_, _, _) -> failwith "<case>"
        | TLet (_, _, _) -> failwith "<case>"
        | TAlloc (_, _) -> failwith "<case>"
        | TAssign (l, n, r) ->
            TAssign(l, n, go r)

        | TExtern _
        | TConstr _
        | TInt _
        | TBool _
        | TUnit _
        | TParamRef _
        | TError _
        | TVar _ -> t

    and def_initializers
        (subst: IR.t Symbol.Hashtbl.t)
        (path: Symbol.t list)
        ((sym, rhs): Symbol.t * IR.t)
        : ((Symbol.t list * t) list) =
        let new_path = sym::path in
        let def_initializers = def_initializers subst in
        match rhs with
        | TModule(_, _, defs) ->
            let loc = IR.loc rhs in
            let ty = IR.ty rhs in
            let (hd, tail) = match List.rev new_path with
                | (hd::tl) -> hd, tl
                | _ -> panicf "compiler bug" in
            let init = IR.t_var loc ty hd in
            let path_term =
                tail
                |> List.fold
                    ~init
                    ~f:(IR.t_prop loc ty)
            in
            Hashtbl.add_exn subst ~key:sym ~data:path_term;
            defs
            |> Array.to_list
            |> List.concat_map ~f:(def_initializers new_path)
        | TExtern _
        | TConstr _
        | TInt _
        | TBool _
        | TUnit _
        | TParamRef _
        | TError _
        | TVar _
        | _ -> [new_path, rhs]

    and go_module name_opt loc ty defs =
        let make_new_symbol _ = make_symbol
            (sprintf "$tmp_%i" (next_mod_id())) in
        let name = name_opt |> Option.or_else ~f:make_new_symbol in
        let subst = Symbol.Hashtbl.of_alist_exn [] in
        let symbol_mapping = Symbol.Hashtbl.of_alist_exn [] in
        let new_symbols = Symbol.Hash_set.create () in
        let module B = Graph.Builder(Symbol) in
        let flattened_assignments = defs
            |> Array.to_list
            |> List.concat_map ~f:(def_initializers subst (name::[]))
            |> List.to_array
            |> Array.map ~f:(fun (p, rhs) ->
                let l = List.rev p in
                let hd = List.hd_exn l in
                let tl = List.tl_exn l in
                (hd, tl), rhs)
        in
        let path_mapping = Symbol.Hashtbl.of_alist_exn [] in
        let init_function_symbols = Vector.Builder.empty () in
        flattened_assignments
            |> Array.iter ~f:(fun ((hd, tl), rhs) ->
                let last_sym = tl |> List.last |> Option.value ~default:hd in
                let new_sym = make_symbol (Symbol.(
                    sprintf "$tmp_%s_%i"
                        (tl |>
                            List.fold ~init:(Symbol.name hd)
                            ~f:(fun prev current ->
                                sprintf "%s_%s" prev (Symbol.name current)))
                        (id last_sym))
                ) in

                if last_sym |> Symbol.name |> String.equal "init"
                then
                    Vector.Builder.push new_sym init_function_symbols;

                let new_rhs = IR.t_var loc (IR.ty rhs) new_sym in
                Hashtbl.add_exn symbol_mapping
                    ~key:last_sym ~data:new_sym;
                Hashtbl.add_exn subst
                    ~key:last_sym ~data:new_rhs;
                Hash_set.strict_add_exn new_symbols new_sym;
                B.add_vertex new_sym;
                Hashtbl.add_exn path_mapping
                    ~key:new_sym ~data:(hd, tl);
            );
        let assignments_map = flattened_assignments
            |> Array.map ~f:(fun ((hd, tl), rhs) ->
                let last_sym = tl |> List.last |> Option.value ~default:hd in
                let new_sym = Hashtbl.find_exn symbol_mapping last_sym in
                new_sym, apply_subst subst rhs
            )
            |> Array.to_list
            |> Symbol.Hashtbl.of_alist_exn
            in


        assignments_map |> Hashtbl.iteri ~f:(fun ~key:s ~data:rhs ->
            let child = s in
            let hash_set = Symbol.Hash_set.create () in
            vars new_symbols hash_set rhs;
            hash_set |> Hash_set.iter ~f:(fun parent ->
                B.add_edge ~parent ~child
            );
            ()
        );

        let module G = B.Build() in
        let sccs = Graph.scc (module G) in
        Vector.rev_inplace sccs;

        let assignments = Vector.Builder.empty () in

        sccs |> Vector.iter ~f:(fun scc ->
            if Vector.length scc > 1
            then (
                printf "%s" (Vector.pp (Symbol.pp) scc);
                panicf "cyclic dependency"
            );
            Vector.iter scc ~f:(fun sym ->
                let a = (sym, Hashtbl.find_exn assignments_map sym) in
                Vector.Builder.push a assignments;
            );
        );
        let assignments = assignments |> Vector.of_builder in

        let initializations = path_mapping |> Hashtbl.to_alist
            |> List.to_array
            |> Array.map ~f:(fun (s, (hd, tl)) ->
                (make_symbol (sprintf "_%i" (Symbol.id s))), IR.TAssign(loc, (hd, tl), IR.t_var loc ty s)
            )
        in

        let init_function_symbols = 
            init_function_symbols |> Vector.of_builder in
        Array.rev_inplace init_function_symbols;

        let init_calls = init_function_symbols
            |> Array.map ~f:(fun s ->
                let sym = make_symbol (sprintf "init_%i" (Symbol.id s)) in
                let call = IR.t_call
                    loc
                    IR.unit_ty
                    (IR.t_var loc
                        (IR.arrow loc [||] IR.unit_ty) s
                    ) [||] in
                sym, call
            )
        in

        IR.t_let loc (Array.concat [
            [| name, IR.TAlloc(loc, ty) |];
            assignments;
            initializations;
            init_calls;
        ]) (IR.t_var loc ty name)
    in
    go root
