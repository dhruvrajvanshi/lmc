open Prelude

type t = {
    mutable next_id: Int.t
}

let make() = { next_id = Int.of_int 1 }

let next_id self =
    let id = self.next_id in
    self.next_id <- id + 1;
    id

