open Prelude
open Syntax

type t = {
    parent_map: Node.t Int.Hashtbl.t;
    def_node_map: Node.t Symbol.Hashtbl.t;
    associated_scope_map: Scope.t Symbol.Hashtbl.t;
    param_symbols: Loc.t Symbol.Hashtbl.t;
    global_symbols: Symbol.Hash_set.t;
}

let global_symbols self = self.global_symbols

let def_node_map t = t.def_node_map
let parent_map t = t.parent_map

let is_global self s = Hash_set.mem self.global_symbols s

let associated_scope_map t = t.associated_scope_map

let associated_scope_of_symbol t s =
    let map = associated_scope_map t in
    Hashtbl.find map s

let resolve_prop_of_symbol t s prop =
    s
    |> associated_scope_of_symbol t
    |> Option.concat_map ~f:(fun scope ->
        Scope.get_local scope ~name:prop
    )

let decl_node_of_symbol st s =  Hashtbl.find (def_node_map st) s

let is_param self s = Hashtbl.mem self.param_symbols s
let param_loc self s = Hashtbl.find self.param_symbols s

let make ~parent_map ~def_node_map ~associated_scope_map ~param_symbols ~global_symbols =
    { parent_map; def_node_map; associated_scope_map; param_symbols; global_symbols }
