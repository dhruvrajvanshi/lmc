open LibCore
open Prelude

type t

val make: id_gen:IdGen.t -> input:String.t -> path:Path.t  -> t

val next_token: t -> Syntax.Token.t
