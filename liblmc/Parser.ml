open Syntax
open LibCore
open Prelude
open Token

let recovery_tokens = Token.Variant.Hash_set.of_list [
    EOF;
    SEMICOLON;
]

let empty_pos = Pos.make 0 0

let is_dir_or_source_file path =
    Path.is_directory path || Path.has_extension ~extn:"lm" path


let path_module_name = Path.(file_name >>> chop_extension)

let make_path_meta
    ~id_gen
    ~(path: Path.t)
    ~(scope): Meta.t =
    let start = empty_pos in
    let stop = empty_pos in
    let loc = Loc.make ~path ~start ~stop in
    let id = IdGen.next_id id_gen in
    Meta.make ~id ~loc ~scope ~flags:MetaFlags.empty
let parse_source_file ~path ~parent_scope ~(id_gen) : SourceFile.t =
    let _input = FS.read_to_string path in
    let ignore (_: Token.t) = () in
    let next_token: unit -> Token.t = 
        let lexer = Lexer.make ~input:_input ~path ~id_gen in
        (fun _ -> Lexer.next_token lexer)
    in

    let _current_token: Token.t ref = ref (next_token()) in
    let _lookahead: Token.t Option.t ref = ref None in

    let _scopes: Scope.t list ref = ref [parent_scope] in

    let current_token () = !_current_token in
    let scopes() = !_scopes in
    let current_scope () = scopes() |> List.hd_exn in
    let lookahead () = !_lookahead in

    let with_scope f =
        let push_scope () =
            let parent = current_scope () in
            let scope = Scope.empty parent in
            _scopes := scope :: scopes()
        in
        let pop_scope () =
            let tl = scopes() |> List.tl_exn in
            let hd = scopes() |> List.hd_exn in
            _scopes := tl;
            hd
        in
        push_scope ();
        let scope = current_scope () in
        let result = f scope in
        pop_scope () |> Caml.ignore;
        result
    in

    let make_meta ?(flags=MetaFlags.empty) loc =
        let id = id_gen |> IdGen.next_id in
        let scope = current_scope () in
        Meta.make ~id ~loc ~scope ~flags
    in
    let advance () =
        let tok = current_token () in
        (match lookahead () with
        | Some(t) ->
            _current_token := t;
            _lookahead := None
        | None ->
            _current_token := next_token ()
        );
        tok

    in

    let peek () =
        match lookahead() with
        | Some(t) ->
            t
        | None ->
            let t = next_token() in
            _lookahead := Some(t);
            t
    in

    let error str =
        let t = current_token () in
        printf "%s: %s\n\n"
            (Token.loc t |> Loc.show)
            str
    in
    let errorf fmt = Format.ksprintf error fmt
    in

    let expect expected =
        let t = current_token () in
        if Token.is t expected
        then advance ()
        else begin
            errorf
                "%s: Unexpected token: Found %s:'%s', Expected %s\n"
                (loc t |> Loc.show)
                (Variant.show t.variant)
                (lexeme t)
                (Variant.show expected)
            ;

            let variant = ERROR in
            let lexeme = "" in
            let start = current_token () |> Token.start_pos in
            let stop = start in
            let loc = Loc.make ~path ~start ~stop in
            let meta = make_meta loc in
            Token.make ~meta ~lexeme ~variant
        end

    in


    let current_variant = current_token >>> Token.variant in

    let recover_from_error () =
        let rec go () =
            if Token.is (current_token ()) RPAREN
            then current_token ()
            else if Hash_set.mem recovery_tokens (current_variant ())
            then advance ()
            else begin
                advance () |> ignore;
                (go [@tailcall]) ()
            end
        in
        let start = current_token () |> Token.loc in
        advance () |> ignore;
        let stop = go () |> Token.loc in
        let loc = Loc.between start stop in
        make_meta loc
    in
    let parse_ident () =
        let tok = expect ID in
        let meta = make_meta (Token.loc tok) in
        let name = Token.lexeme tok in
        Ident.make meta name
    in
    let parse_binding_ident scope =
        match (current_variant ()) with
        | UNDERSCORE ->
            let loc = advance () |> Token.loc in
            let meta = make_meta loc in
            BindingIdent.wildcard meta scope
        | _ ->
            let ident = parse_ident () in
            let meta = make_meta (Ident.loc ident) in
            BindingIdent.name meta scope ident
    in
    
    let is_labeled_arrow_param_expected () =
        (Token.is (current_token ()) ID) && Token.is (peek ()) COLON
    in

    let rec parse_comma_seperated_tail builder f =
        match current_variant () with
        | COMMA ->
            advance () |> ignore;
            let item = f () in
            Vector.Builder.push item builder;
            parse_comma_seperated_tail builder f
        | _ -> Vector.of_builder builder
    in

    let parse_comma_seperated_list ~stop ?(hd=None) f =
        let builder = Vector.Builder.empty() in
        (match hd with
        | Some(t) ->
            Vector.Builder.push t builder;
            if (Token.is (current_token ()) COMMA)
            then advance () |> ignore
        | _ -> ()
        );
        if  Token.is (current_token ()) stop
        then Vector.of_builder builder
        else
            let hd = f () in
            Vector.Builder.push hd builder;
            parse_comma_seperated_tail builder f
    in
    let parse_var_term () =
        let ident = parse_ident () in
        let meta = make_meta (Ident.loc ident) in
        Term.var meta ident
    in
    let rec parse_term (): Term.t =
        let hd = match current_variant () with
        | ID -> parse_var_term ()
        | PANIC -> parse_panic ()
        | INT -> parse_int_term ()
        | MODULE -> parse_module_term ()
        | EXTERN -> parse_extern_term ()
        | IF -> parse_if_term ()
        | LPAREN ->
            parse_lparen_term ()
        | _ ->
            errorf
                "Term expected; Found token '%s'"
                (lexeme (current_token ()));
            let meta = recover_from_error () in
            Term.error meta

        in
        parse_term_tail hd

    and parse_panic _ =
        let start = expect PANIC |> Token.loc in
        let stop = start in
        let loc = Loc.between start stop in
        let meta = make_meta loc in
        Term.panic meta


    and parse_lparen_term () =
        let lparen = expect LPAREN in
        if is_labeled_arrow_param_expected ()
        then (
            let start = Token.loc lparen in
            with_scope (fun scope ->
                let params = parse_arrow_params scope in
                expect RPAREN |> ignore;
                expect ARROW |> ignore;
                let rhs = parse_ty_annotation () in

                let stop = TyAnnotation.loc rhs in
                let loc = Loc.between start stop in
                let meta = make_meta loc in
                Term.arrow meta scope params rhs
            )
        )
            
        else (
            let hd = parse_term () in
            (match current_variant () with
            | COMMA ->
                let annot_meta = make_meta (Term.loc hd) in
                let hd = Some (TyAnnotation.make annot_meta hd) in
                with_scope (fun scope ->
                    let params = parse_arrow_params ~hd scope in
                    expect RPAREN |> ignore;
                    expect ARROW |> ignore;
                    let ret_typ = parse_ty_annotation () in
                    let start = Token.loc lparen in
                    let stop = TyAnnotation.loc ret_typ in
                    let meta = make_meta (Loc.between start stop) in
                    Term.arrow meta scope params ret_typ
                )
                
                
            | _ ->
                expect RPAREN |> ignore;
                hd
            )
        )

    and parse_int_term () =
        let tok = expect INT in
        let value = int_of_string (Token.lexeme tok) in
        let meta = make_meta (Token.loc tok) in
        Term.l_int meta value

    and parse_if_term () =
        let start = expect IF |> Token.loc in
        expect LPAREN |> ignore;
        let cond = parse_term () in
        expect RPAREN |> ignore;
        let t = parse_term () in
        expect ELSE |> ignore;
        let f = parse_term () in
        let stop = Term.loc f in
        let loc = Loc.between start stop in
        let meta = make_meta loc in
        Term.t_if meta cond t f

    and parse_extern_term () =
        let start = expect EXTERN |> Token.loc in
        let ident = parse_ident () in
        let stop = ident |> Ident.loc in
        let loc = Loc.between start stop in
        let meta = make_meta loc in
        Term.extern meta ident

    and parse_arrow_params
        ?(hd:(TyAnnotation.t option)=None)
        (scope: Scope.t)
        =
        let hd = Option.map hd ~f:Term.Arrow.Param.unlabeled in
        parse_comma_seperated_list ~hd ~stop:RPAREN parse_arrow_param


    and parse_term_tail hd =
        let start = Term.loc hd in
        match current_variant () with
        | DOT ->
            advance () |> ignore;
            let lhs = hd in
            let rhs = parse_ident () in
            let stop  = Ident.loc rhs in
            let loc = Loc.between start stop in
            let meta = make_meta loc in
            Term.prop meta lhs rhs |> parse_term_tail

        | LPAREN ->
            advance () |> ignore;
            let args = parse_call_args () in
            let stop = expect RPAREN |> Token.loc in
            let loc = Loc.between start stop in
            let meta = make_meta loc in
            Term.call meta hd args |> parse_term_tail
        | ARROW ->
            advance () |> ignore;
            let annot = TyAnnotation.make (make_meta (Term.loc hd)) hd in
            (* Since the arrow param is a term, it can't possibly bind
               a variable so, even though lexically, the scope should
               start before the already parsed term, we can start it here.
            *)
            with_scope (fun scope ->
                let rhs = parse_ty_annotation () in
                let param = Term.Arrow.Param.unlabeled annot in
                let params = param |> Vector.singleton in
                let loc = Loc.between (Term.loc hd) (TyAnnotation.loc rhs) in
                let meta = make_meta loc in
                Term.arrow meta scope params rhs |> parse_term_tail
            )
            
        | _ -> hd

    and parse_call_args () =
        let stop = RPAREN in
        let args = parse_comma_seperated_list ~stop parse_call_arg in
        args

    and parse_call_arg () =
        let t = current_token () in
        let next = peek () in
        if Token.is t ID && Token.is next EQ
        then
            panic "parse_call_arg: Unhandled case for named call args"
        else
            let tm = parse_term () in
            let arg = Term.Call.Arg.make None tm in
            arg

    and parse_module_term () =
        let start = expect MODULE |> Token.loc in
        with_scope (fun scope ->
            expect LBRACE |> ignore;

            let decls = parse_declarations_till RBRACE in
            let stop = expect RBRACE |> Token.loc in
            let loc = Loc.between start stop in
            let meta = make_meta loc in
            Term.l_module meta scope decls
        )
    
    and parse_ty_annotation (): TyAnnotation.t =
        let tm = parse_term () in
        let meta = make_meta (Term.loc tm) in
        TyAnnotation.make meta tm
    
    and parse_arrow_param (): Term.Arrow.Param.t =
        if is_labeled_arrow_param_expected ()
        then
            let ident = parse_binding_ident (current_scope ()) in
            expect COLON |> ignore;
            let annot = parse_ty_annotation () in
            Term.Arrow.Param.labeled ident annot
        else
            let annot = parse_ty_annotation () in
            Term.Arrow.Param.unlabeled annot

    and parse_optional_annotation () =
        match current_variant () with
        | COLON ->
            advance () |> ignore;
            Some(parse_ty_annotation ())
        | _ -> None

    and parse_params () =
        parse_comma_seperated_list ~stop:RPAREN parse_param

    and parse_param () =
        let bi = parse_binding_ident (current_scope ()) in
        let annot = (
            match (current_variant ()) with
            | COLON ->
                advance () |> ignore;
                Some(parse_ty_annotation ())
            | _ -> None
            )
        in
        Param.make bi annot

    and parse_def (): Declaration.t =
        let open Token in
        let (start, name) = match current_variant() with
            | ID ->
                let name = parse_binding_ident (current_scope ()) in
                let loc = BindingIdent.loc name in
                loc, name
            | _ ->
                let loc = expect Token.DEF |> Token.loc in
                let name = parse_binding_ident (current_scope ()) in
                loc, name
        in
        with_scope (fun scope ->
            let (params, flags) = (match current_variant () with
            | LPAREN ->
                advance () |> ignore;
                let params = parse_params () in
                expect RPAREN |> ignore;
                params, MetaFlags.has_param_list
            | _ -> Vector.empty, MetaFlags.empty
            ) in
            let annotation = parse_optional_annotation () in
            expect EQ |> ignore;
            let rhs = parse_term () in
            let stop = Term.loc rhs in
            expect SEMICOLON |> ignore;
            let loc = Loc.between start stop in
            let meta = make_meta ~flags loc in
            Declaration.def meta ~scope ~name ~params ~annotation ~rhs
        )

    and parse_declaration self =
        let open Token in
        match current_variant self with
        | DEF ->
            parse_def self
        | ID ->
            parse_def self
        | _ ->
            errorf "Declaration expected; Found token '%s'"
                (lexeme (current_token self));
            let meta = recover_from_error self in
            Declaration.error meta


    and parse_declarations_till (variant: Token.variant): Declaration.t Vector.t =
        let decls = Vector.Builder.empty () in
        let rec go () =
            match current_token () with
            | t when Token.is t variant -> ()
            | _ ->
                let decl = parse_declaration () in
                Vector.Builder.push decl decls;
                (go [@tailcall]) ()
        in
        go ();
        decls |> Vector.of_builder

    in

    let sf = with_scope (fun scope ->
        let declarations = parse_declarations_till Token.EOF in
        let name = path_module_name path in
        SourceFile.make ~scope ~declarations ~name
    ) in
    ensure ((List.length (scopes())) = 1)
        "unbalanced scopes";
    sf
    


let rec parse_dir
    (path: Path.t)
    ~(parent_scope: Scope.t)
    ~(id_gen: IdGen.t)
    : String.t * CompilationUnit.t =
    let name = path_module_name path in

    let scope = Scope.empty parent_scope in
    let units = FS.read_dir path
        |> Vector.filter ~f:(is_dir_or_source_file)
        |> Vector.map ~f:(parse_unit
            ~parent_scope:scope
            ~id_gen
        )
        |> Vector.to_list
        |> Hashtbl.of_alist_exn (module String) in

    let meta = make_path_meta ~id_gen ~path ~scope:parent_scope in
    name, CompilationUnit.directory meta ~name ~units ~scope
    
and make_unit_binding_ident ~id_gen ~path ~scope ~name =
    let ident_meta = make_path_meta ~id_gen ~path ~scope in
    let bi_meta = make_path_meta ~id_gen ~path ~scope in
    let ident = Ident.make ident_meta name in
    BindingIdent.name bi_meta scope ident

and parse_unit
    (path: Path.t)
    ~(parent_scope: Scope.t)
    ~(id_gen: IdGen.t) 
    =
    let name = path_module_name path in
    if Path.is_directory path
    then parse_dir path
        ~id_gen
        ~parent_scope
    else
        let sf = parse_source_file ~id_gen ~path ~parent_scope in
        let meta = make_path_meta ~id_gen ~path ~scope:parent_scope in
        name, CompilationUnit.source_file meta sf

let parse_program
    ~id_gen
    ~(dirs)
    : Program.t =
    let scope = Scope.root () in
    let units = dirs
        |> List.concat_map ~f:(FS.read_dir >>> Array.to_list)
        |> List.map ~f:(parse_unit
            ~parent_scope:scope
            ~id_gen
        )
        |> Hashtbl.of_alist_exn (module String) in
    Program.make units scope
