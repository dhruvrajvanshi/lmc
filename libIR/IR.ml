open LibCore
open Prelude

module Symbol = LibCore.Symbol

type symbol = Symbol.t
type loc = Loc.t

module Self = struct
    type param = Symbol.t option
    type tm_path = Symbol.t * Symbol.t list
    type term =
        | TVar of loc * ty * symbol
        | TInt of loc * int
        | TBool of loc * bool
        | TUnit of loc
        | TCall of loc * ty * term * term array
        | TModule of loc * ty * (Symbol.t * term) array
        | TModuleType of loc * (Symbol.t * ty) array
        | TProp of loc * ty * term * Symbol.t
        | TLambda of loc * ty * param array * term
        | TConstr of loc * symbol
        | TArrow of loc * arrow_param array * ty
        | TPanic of loc * ty * term
        | TExtern of loc * ty * string
        | TIf of loc * ty * term * term * term
        | TParamRef of loc * ty * Symbol.t
        | TLazy of loc * term
        | TForce of loc * ty * term
        | TLet of loc * (Symbol.t * term) array * term
        | TAlloc of loc * ty
        | TAssign of loc * tm_path * term
        | TError of loc

    and arrow_param = Symbol.t option * term

    and ty = term

    type t = term

    let find_member_by_name members name =
        members
        |> Vector.find ~f:(fst >>> Symbol.name >>> String.equal name)

    let loc = function
        | TVar (l, _, _) -> l
        | TInt (l, _) -> l
        | TBool (l, _) -> l
        | TUnit l-> l
        | TCall (l, _, _, _) -> l
        | TModule (l, _, _) -> l
        | TModuleType (l, _) -> l
        | TProp (l, _, _, _) -> l
        | TLambda (l, _, _, _) -> l
        | TConstr (l, _) -> l
        | TArrow (l, _, _) -> l
        | TPanic (l, _, _) -> l
        | TExtern (l, _, _) -> l
        | TIf (l, _, _, _, _) -> l
        | TParamRef(l, _, _) -> l
        | TLazy(l, _) -> l
        | TForce(l, _, _) -> l
        | TLet(l, _, _) -> l
        | TAlloc(l, _) -> l
        | TAssign(l, _, _) -> l
        | TError(l) -> l


    let rec pp_t_map m = 
        m
        |> Vector.pp ~sep:";\n"
            (fun (s, t) -> sprintf "  %s = %s" (Symbol.name s) (pp t))
    and pp_path (hd, tl) =
        sprintf "%s%s" (Symbol.name hd)
            (tl |> List.(
                map ~f:(Symbol.name >>> sprintf ".%s")
                >>> fold ~init:"" ~f:(^)
            ))
    and pp = function
        | TVar (_, _, s) -> Symbol.name s
        | TInt (_, i) -> string_of_int i
        | TBool(_, b) -> string_of_bool b
        | TUnit(_) -> "unit"
        | TCall (_, _, f, args) -> sprintf "%s(%s)" (pp f)
            (Vector.pp pp args)
        | TModule (_, _, m) ->
            sprintf "module {\n%s\n}" (pp_t_map m)
        | TModuleType (_, m) ->
            sprintf "module type {\n%s\n}" (pp_t_map m)
        | TProp (_, _, t, s) ->
            sprintf "%s.%s" (pp t) (Symbol.name s)
        | TLambda (_, _, params, tm) ->
            sprintf "\\%s => %s"
                (Vector.pp ~sep:" " (
                    Option.map ~f:Symbol.name
                    >>> Option.value ~default:"_"
                ) params)
                (pp tm)
        | TConstr (_, s) -> Symbol.name s
        | TArrow (_, p, r) ->
            sprintf "((%s) -> %s)"
                (Vector.pp (function
                    | Some(l), t -> sprintf "%s: %s" (Symbol.name l) (pp t)
                    | None, t -> pp t
                    )
                    p)
                (pp r)
        | TPanic (_, _, t) -> sprintf "panic %s" (pp t)
        | TExtern (_, _, s) -> sprintf "extern %s" s
        | TIf (_, _, c, t, f) ->
            sprintf "if (%s) %s else %s"
                (pp c)
                (pp t)
                (pp f)
        | TParamRef(_, _, s) ->
            Symbol.name s
        | TLazy(_, tm) ->
            sprintf "lazy %s" (pp tm)
        | TForce(_, _, t) ->
            sprintf "force %s" (pp t)

        | TAlloc(_, ty) ->
            sprintf "alloc %s" (pp ty)
        | TLet(_, bindings, t) ->
            let pp_binding (name, rhs) =
                sprintf "let %s = %s in\n"
                    (Symbol.name name)
                    (pp rhs)
            in
            let bindings = Array.(
                bindings
                |> map ~f:pp_binding
                |> fold ~init:"" ~f:(^)
            )
            in
            sprintf "%s\n%s"
                bindings
                (pp t)
        | TAssign(_, path, tm) ->
            sprintf "%s = %s" (pp_path path) (pp tm)
        | TError(_) -> "<ERROR>"
end

include Self

module Make(Primitives: Primitives.T) = struct
    include Self
    include Primitives
    type id = int

    let builtin_loc =
        let start = Pos.make 0 0 in
        let stop = start in
        Loc.make ~path:"builtin" ~start ~stop

    let constr loc sym = TConstr(loc, sym)

    let type_ty = constr builtin_loc  Primitives.type_ty_symbol

    let int_ty = constr builtin_loc Primitives.int_ty_symbol

    let bool_ty = constr builtin_loc Primitives.bool_ty_symbol

    let unit_ty = constr builtin_loc Primitives.unit_ty_symbol

    let lazy_constr = constr builtin_loc Primitives.lazy_ty_symbol

    let arrow loc p t = TArrow(loc, p, t)


    let prop loc ty tm name =
        TProp(loc, ty, tm, name)

    (* let symbol_map_to_str_map =
        Hashtbl.to_alist
        >>> List.map ~f:(fun (s, _) -> Symbol.name s, s)
        >>> String.Hashtbl.of_alist_exn
 *)
    let t_module_type loc bindings =
        TModuleType(loc, bindings)

    let t_var loc ty sym =
        TVar(loc, ty, sym)

    let t_force loc ty t =
        TForce(loc, ty, t)
        

    let t_lambda loc ty params rhs =
        TLambda(loc, ty, params, rhs)

    let t_error loc = TError(loc)

    let t_bool loc b = TBool(loc, b)

    let t_unit loc = TUnit(loc)

    let t_param_ref loc ty sym = TParamRef(loc, ty, sym)

    let t_int loc i = TInt(loc, i)

    let t_extern loc ty name = TExtern(loc, ty, name)

    let t_panic loc ty tm = TPanic(loc, ty, tm)

    let t_call loc ty f args = TCall(loc, ty, f, args)

    let t_if loc ty c t f = TIf(loc, ty, c, t, f)

    let t_prop loc ty t s = TProp(loc, ty, t, s)

    let lazy_ty loc t =
        t_call loc type_ty lazy_constr [|t|]

    let t_lazy loc tm =
        TLazy(loc, tm)

    let t_let loc bindings rhs =
        TLet(loc, bindings, rhs)

    let rec t_module loc terms =
        let typ =
            Array.map terms ~f:(fun (s, t) -> s, ty t)
            |> t_module_type loc
        in
        TModule(loc, typ, terms)


    and ty = function
        | TVar (_, ty, _) -> ty
        | TInt _ -> int_ty
        | TBool _ -> bool_ty
        | TUnit _ -> unit_ty
        | TCall (_, ty, _, _) -> ty
        | TModule (_, ty, _) -> ty
        | TProp (_, ty, _, _) -> ty
        | TLambda (_, ty, _, _) -> ty
        | TExtern(_, ty, _) -> ty
        | TIf(_, ty, _, _, _) -> ty
        | TPanic(_, ty, _) -> ty
        | TModuleType _
        | TConstr _
        | TArrow _ -> type_ty
        | TLazy(_, t) ->
            let loc = loc t in
            lazy_ty loc t
        | TAlloc(loc, _) ->
            unit_ty
        | TForce(_, ty, _) ->
            ty
        | TParamRef(_, ty, _) -> ty
        | TLet(_, _, t) -> ty t
        | TAssign(_, _, _) -> unit_ty
        | TError(_) -> unit_ty

    let extern_values = String.Hashtbl.of_alist_exn [
        "__lmc__Type_t", type_ty;
        "__lmc__Int_t", int_ty;
        "__lmc__Bool_t", bool_ty;
        "__lmc__Unit_t", unit_ty;
        "__lmc__Lazy_t_constr", lazy_constr;
    ]
end