open LibCore
open Prelude
open LibIR

module J = struct
    type t =
        | Int of int
        | Var of string
        | Obj of (string * t) array
        | Bool of bool
        | Undefined
        | Const of string * t
        | Decls of t array
        | Function of string * string array * t
        | Call of t * t array
        | Block of t array
        | Return of t
        | Prop of t * string
        | Ternary of t * t * t
        | Assign of t * t
        | Throw of t
        | ArrowFunc of string array * t
        | Void of t
        | LineComment of string

    let int x = Int x
    let var v = Var v

    let obj o = Obj o

    let const n v = Const(n, v)

    let decls d = Decls d

    let line_comment c = LineComment c

    let call f args = Call(f, args)

    let func name pref body =
        Function(name, pref, body)

    let bool b = Bool(b)

    let undefined = Undefined

    let prop p r = Prop(p, r)

    let ternary p t f = Ternary(p, t, f)

    let throw t = Throw t

    let ppi i = Vector.create ~len:(i) "  "
        |> Vector.fold ~init:"" ~f:(^)

    let rec pp ?(i=0) (t: t) =
        match t with
        | Int i -> string_of_int i
        | Var n -> n
        | Obj items ->
            let pp_item (n, r) =
                sprintf "%s: %s" n (pp ~i:(i + 1) r) in
            let sep = sprintf ",\n%s" (ppi (i + 1)) in
            let body = Vector.pp ~sep pp_item items in
            sprintf "({\n%s%s\n%s})"
                (ppi (i + 1))
                body
                (ppi i)
        | Const (n, r) -> sprintf "const %s = %s;" n (pp ~i r)
        | Decls d ->
            d |> Vector.fold ~init:"" ~f:(fun prev d ->
                sprintf "%s%s%s\n" prev (ppi i) (pp ~i d)
            )
        | Assign(lhs, rhs) ->
            sprintf "%s = %s" (pp lhs) (pp rhs)
        | Function (n, ps, t) ->
            let ps = Vector.pp ~sep:", " id ps in
            let t = pp ~i:(i + 1) t in
            sprintf "(function %s(%s) {\n%sreturn %s;\n%s})"
                n
                ps
                (ppi (i + 1))
                t
                (ppi i)
        | ArrowFunc (ps, t) ->
            let ps = Vector.pp ~sep:", " id ps in
            let t = pp ~i:(i + 1) t in
            sprintf "((%s) =>\n%s%s)"
                ps
                (ppi (i + 1))
                t
        | Block ms ->
            let members =
                ms |> Array.map ~f:(pp ~i:(i + 1))
                |> Array.fold ~init:"" ~f:(fun prev s ->
                    sprintf "%s%s%s;\n"
                        prev
                        (ppi (i + 1))
                        s
                ) in
            sprintf "{\n%s%s\n}"
                members
                (ppi i)
        | Void t -> sprintf "(void (%s))" (pp t)
        | Return t -> sprintf "return %s" (pp t)
        | Call (f, args) ->
            let f = pp ~i f in
            let args = Vector.pp ~sep:", " (pp ~i:(i + 1)) args in
            sprintf "%s(%s)"
                f
                args
        | Bool true -> "true"
        | Bool false -> "false"
        | Undefined -> "undefined"
        | Prop(p, r) -> sprintf "%s.%s" (pp ~i p) r
        | Ternary (p, t, f) ->
            sprintf "%s\n%s? %s\n%s: %s"
                (pp ~i p)
                (ppi (i + 1))
                (pp ~i:(i + 1) t)
                (ppi (i + 1))
                (pp ~i:(i + 1) f)
        | Throw t -> sprintf "throw %s" (pp ~i t)
        | LineComment c -> sprintf "// %s" c

end

let codegen
    (module Primitives: Primitives.T)
    (tm: IR.term) out_dir =
    let module IR = IR.Make(Primitives) in


    let symbol_name s =
        Symbol.name s
    in

    let rec gen_term (tm: IR.t) = IR.(match tm with
        | TVar (_, _, v) -> J.var (symbol_name v)
        | TInt (_, t) -> J.int t
        | TBool (_, b) -> J.bool b
        | TUnit _ -> J.undefined
        | TCall (_, _, f, args) -> gen_call f args
        | TModule (l, ty, members) -> gen_module l ty members
        | TModuleType (_, _) -> failwith "<case ModuleType>"
        | TProp (_, t, lhs, rhs) ->
            gen_prop t lhs rhs
        | TLambda (l, ty, ps, b) -> gen_lambda l ty ps b
        | TConstr (_, _) -> failwith "<case TConstr>"
        | TArrow (_, _, _) -> failwith "<case Arrow>"
        | TPanic (_, _, t) -> gen_panic t
        | TExtern (_, _, n) -> J.var n
        | TIf (_, _, c, t, f) -> gen_if c t f
        | TParamRef (_, _, _) -> failwith "<case ParamRef>"
        | TAlloc(loc, ty) -> gen_alloc ty
        | TLazy (l, t) -> gen_lazy l t
        | TForce (l, _, t) -> gen_force l t
        | TLet(loc, bs, body) -> gen_t_let loc bs body
        | TAssign(loc, lhs, rhs) -> gen_assign loc lhs rhs
        | TError _ -> failwith "<case Error>")

    and gen_lambda l ty ps b =
        let params = ps |> Array.mapi ~f:(fun i s_opt ->
            s_opt
            |> Option.map ~f:symbol_name
            |> Option.value ~default:(sprintf "_%i" i)
        ) in
        J.ArrowFunc(params, gen_term b)

    and gen_force l t =
        J.call (J.prop (gen_term t) "force") [||]

    and gen_module l ty members =
        members
        |> Array.map
            ~f:(fun (sym, t) ->
                sym |> Symbol.name, gen_term t)
        |> J.obj

    and gen_assign l (hd, tl) rhs =
        let lhs = tl
            |> List.map ~f:(Symbol.name)
            |> List.fold
                ~init:(J.var (symbol_name hd))
                ~f:J.prop in
        let rhs = gen_term rhs in
        J.Void(J.Assign(lhs, rhs))

    and gen_lazy l t =
        J.call (J.var "$lazy") [| J.ArrowFunc([||], gen_term t) |]

    and gen_t_let loc bs body =
        let consts = bs
            |> Array.map ~f:(fun (n, r) ->
                J.const (symbol_name n) (gen_term r))
            |> J.decls in
        let ret = J.Return(gen_term body) in
        let stmts = J.Decls [| consts; ret |] in
        J.Call(
            J.ArrowFunc([||], J.Block([| stmts |])),
            [||]
        )

    and gen_panic t =
        J.call (J.var "__lmc__panic") [| gen_term t |]

    and gen_if c t f =
        let g = gen_term in
        let c = g c in
        let t = g t in
        let f = g f in
        J.ternary c t f


    and gen_prop t lhs rhs =
        let lhs = gen_term lhs in
        J.prop lhs (Symbol.name rhs)

    and gen_call f args =
        J.call
            (gen_term f)
            (args
            |> Vector.map ~f:gen_term
            )

    and gen_alloc (typ: IR.ty): J.t = IR.(match typ with
        | TModuleType(_, defs) ->
            J.obj (Array.map defs ~f:(fun (s, t) ->
                (Symbol.name s), gen_alloc t
            ))
        | _ -> J.undefined
    )
    in

    let j = gen_term tm in
    
    let body = J.pp j in
    let out_path = Path.join_2 out_dir "out.js" in
    let std = FS.read_to_string (Path.join ["libBackend"; "libCodegen"; "std.js"]) in
    let body = sprintf "%s\n%s" std body in
    printf "writing output\n";
    FS.write_string out_path body;
    printf "compilation done\n"