'use strict'
const __lmc__Type_t = 0;
const __lmc__Int_t = 0;
const __lmc__Bool_t = 0;
const __lmc__Unit_t = 0;
function __lmc__panic(f) {
    throw new Error(f);
}
function __lmc__Bool_eq(b1, b2) {
    return b1 == b2
}
function __lmc__Bool_or(b1, b2) {
    return b1 || b2
}
function __lmc__Bool_and(b1, b2) {
    return b1 && b2
}
function __lmc__Bool_not(b) {
    return !b
}
function __lmc__Int_plus(b1, b2) {
    return b1 + b2
}
function __lmc__Int_eq(b1, b2) {
    return b1 == b2
}
const unit = undefined;
