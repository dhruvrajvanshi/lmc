open LibLMC
open LibCore
open Prelude

let () =
    Sys.chdir "../../..";
    let dirs = [ "test/execution"; "Std" ] in
    let out_dir = "test_output" in
    Compiler.compile ~dirs ~out_dir;
    let out_file = Path.join [out_dir; "out.js"] in
    let status = Sys.command (sprintf "node %s" out_file) in
    OUnit2.assert_bool "Expected node to return status 0" (status = 0)